### Dependencias

Para el funcionamiento de la servicio OTINFO es necesario disponer de:
- una base de datos Postgres + PostGIS
- el servicio web SpatialSearchService (https://gitlab.com/igear_publico/spatialsearchservice) que permite realizar operaciones espaciales entre una determinada geometría y una serie de capas geográficas almacenadas en PostGIS

### Instalación

Para instalar otinfows hay que generar el war con los fuentes proporcionados y desplegarlo en el servidor de aplicaciones.

