package idearium.otinfows.servicios;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import idearium.otinfows.modelo.Expediente;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service
public class ServicioWord {


    public String marcadorActual="";

    public HashMap<String, ResultSet> consultas = new HashMap<>();

    public HashMap<BigInteger, BigInteger> listas = new HashMap<>();

    private final static Logger log = LoggerFactory.getLogger(ServicioWord.class.getName());


    //ultimo 4450
    String decretosValidos = "(4446,4049,4053,4051,4052,4050,4054,4055,4447,4057,4058,4448,4449,4450,4451,4452,132,160,120,148,144,116,152,124,4156,4157)";


    public String relacion =
            "{\"ENP101\":[\"enp/enp101\"],\n" +
            "\"ENP102\":[\"enp/enp102\"],\n" +
            "\"ENP103\":[\"enp/enp103\"],\n" +
            "\"ENP104\":[\"enp/enp104\"],\n" +
            "\"ENP105\":[\"enp/enp105\"],\n" +
            "\"ENP201\":[\"enp/enp201\"],\n" +
            "\"ENP202\":[\"enp/enp202\"],\n" +
            "\"ENP203\":[\"enp/enp203\"],\n" +
            "\"ENP204\":[\"enp/enp204\"],\n" +
            "\"ENP301\":[\"enp/enp301\"],\n" +
            "\"ENP302\":[\"enp/enp302\"],\n" +
            "\"ENP303\":[\"enp/enp303\"],\n" +
            "\"ENP401\":[\"enp/enp302\"],\n" +
            "\"ENP403\":[\"enp/enp403\"],\n" +
            "\"ENP404\":[\"enp/enp404\"],\n" +
            "\"ENP405\":[\"enp/enp405\"],\n" +
            "\"ZENP101\":[\"enp/enp101\"],\n" +
            "\"ZENP103\":[\"enp/enp103\"],\n" +
            "\"ZENP104\":[\"enp/enp104\"],\n" +
            "\"ZENP105\":[\"enp/enp105\"],\n" +
            "\"ZENP202\":[\"enp/enp202\"],\n" +
            "\"ZENP203\":[\"enp/enp203\"],\n" +
            "\"ZENP204\":[\"enp/enp204\"],\n" +
            "\"FALNAU\":[\"biod/biodiversidadFalNau\"],\n" +
            "\"HIEFAS\":[\"biod/aCritHieFas\",\"biod/biodiversidadHieFas\"],\n" +
            "\"GYPBAR\":[\"biod/biodiversidadGypBar\"],\n" +
            "\"AUSPAL\":[\"biod/biodiversidadAusPal\"],\n" +
            "\"MARAUR\":[\"biod/biodiversidadMarAur\"]}";


    @Autowired
    ServicioBD servicioBD;

    static String cTAbstractNumBulletXML =
            "<w:abstractNum xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" w:abstractNumId=\"0\">"
                    + "<w:multiLevelType w:val=\"hybridMultilevel\"/>"
                    + "<w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:numFmt w:val=\"bullet\"/><w:lvlText w:val=\"\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Symbol\" w:hAnsi=\"Symbol\" w:hint=\"default\"/></w:rPr></w:lvl>"
                    + "<w:lvl w:ilvl=\"1\" w:tentative=\"1\"><w:start w:val=\"1\"/><w:numFmt w:val=\"bullet\"/><w:lvlText w:val=\"o\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Courier New\" w:hAnsi=\"Courier New\" w:cs=\"Courier New\" w:hint=\"default\"/></w:rPr></w:lvl>"
                    + "<w:lvl w:ilvl=\"2\" w:tentative=\"1\"><w:start w:val=\"1\"/><w:numFmt w:val=\"bullet\"/><w:lvlText w:val=\"\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"360\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Wingdings\" w:hAnsi=\"Wingdings\" w:hint=\"default\"/></w:rPr></w:lvl>"
                    + "</w:abstractNum>";

    static String cTAbstractNumDecimalXML =
            "<w:abstractNum xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" w:abstractNumId=\"0\">"
                    + "<w:multiLevelType w:val=\"hybridMultilevel\"/>"
                    + "<w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:numFmt w:val=\"decimal\"/><w:lvlText w:val=\"%1\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr></w:lvl>"
                    + "<w:lvl w:ilvl=\"1\" w:tentative=\"1\"><w:start w:val=\"1\"/><w:numFmt w:val=\"decimal\"/><w:lvlText w:val=\"%1.%2\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr></w:lvl>"
                    + "<w:lvl w:ilvl=\"2\" w:tentative=\"1\"><w:start w:val=\"1\"/><w:numFmt w:val=\"decimal\"/><w:lvlText w:val=\"%1.%2.%3\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"360\"/></w:pPr></w:lvl>"
                    + "</w:abstractNum>";


    @Value("${informe}")
    public String informe;

    @Value("${plantilla}")
    String plantilla;

    @Value("${temp.path}")
    String tempPath;

    @Autowired
    ServletContext context;

    @Autowired
    ServicioGeoserver servicioGeoserver;

    @Autowired
    Util util;

    public int numParagraphs = 0;
    public int numTables = 0;

    public XWPFDocument temp;


    public String generarInforme(String cod_sct) throws Exception{


            XWPFDocument template;
            try{
                template = new XWPFDocument(Files.newInputStream(Paths.get(plantilla)));
            }catch (Exception e){
                template = new XWPFDocument(Files.newInputStream(Paths.get(context.getRealPath(plantilla))));
            }


            XWPFDocument document = new XWPFDocument();

            temp = new XWPFDocument();

            numParagraphs = 0;
            numTables = 0;

            List<CTBookmark> bookmarkList;

            List<IBodyElement> elementosMarcador = new ArrayList<>();


            Iterator<IBodyElement> iter = template.getBodyElementsIterator();
            while (iter.hasNext()) {
                IBodyElement elem = iter.next();
                if (elem instanceof XWPFParagraph) {
                    XWPFParagraph paragraph = (XWPFParagraph) elem;

                    bookmarkList = paragraph.getCTP().getBookmarkStartList();

                    if(bookmarkList.size()>0){

                        //Cuando cambia el marcador procesamos el anterior con todos sus elementos
                        procesarMarcador(document,marcadorActual,elementosMarcador,cod_sct, template);

                        //Pasamos al siguiente marcador y reiniciamos la lista de elementos
                        marcadorActual = bookmarkList.get(0).getName();
                        //Borramos el marcador para que no se quede en el documento final
                        bookmarkList.remove(0);
                        elementosMarcador = new ArrayList<>();

                    }
                }

                elementosMarcador.add(elem);
            }

            procesarMarcador(document,marcadorActual,elementosMarcador,cod_sct, template);

            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));

            String ruta = tempPath+cod_sct+"_"+sdf.format(new Date())+".docx";
            FileOutputStream out = new FileOutputStream(ruta);
            document.write(out);
            out.close();
            document.close();
        
            return ruta;
    }


    public void procesarMarcador(XWPFDocument document, String marcador, List<IBodyElement> elementosMarcador, String cod_sct,XWPFDocument template){

        try{
            log.info("------------------------");
            log.info("------------------------");
            log.info(marcador);
            log.info("------------------------");
            log.info("------------------------");

            if(marcador.startsWith("texto_")){
                //Si el marcador empieza por _texto copiamos los elementos tal cual están
                copiarElementos(document,elementosMarcador,template);
            }else if(marcador.startsWith("mapa_")){


                List<String> mapas = servicioGeoserver.obtenerUrlMapa(cod_sct,marcador);

                if(marcador.equals("mapa_figura1") || mapas.size()>2){
                    InputStream is = servicioGeoserver.obtenerImagenConFondo(mapas);
                    if(is!=null){
                        XWPFParagraph paragraph = document.createParagraph();
                        paragraph.createRun().addPicture(is,XWPFDocument.PICTURE_TYPE_PNG, tempPath+"temp.png", Units.toEMU(410), Units.toEMU(410));
                        numParagraphs++;
                        paragraph.setAlignment(ParagraphAlignment.CENTER);

                        List<byte[]> leyendas = servicioGeoserver.obtenerLeyendas(marcador,cod_sct);

                        copiarElementos(document,elementosMarcador,template);

                        for (byte[] img: leyendas){
                            InputStream input =new ByteArrayInputStream(img);

                            paragraph = document.createParagraph();
                            numParagraphs++;

                            BufferedImage imgFile = ImageIO.read(new ByteArrayInputStream(img));
                            paragraph.createRun().addPicture(input,XWPFDocument.PICTURE_TYPE_PNG, tempPath+"temp.png", Units.toEMU(imgFile.getWidth()/2), Units.toEMU(imgFile.getHeight()/2));
                            paragraph.setAlignment(ParagraphAlignment.CENTER);
                        }

                    }
                }
            }else{
                //Para el resto de casos se obtienen los datos mediante sql.
                String sql = servicioBD.obtenerSQL(marcador);

                if(sql != null){
                    sql = sql.replaceAll("@@cod_sct@@",cod_sct);
                    ResultSet rs = servicioBD.execSQL(sql);
                    if(rs!=null){
                        rs.beforeFirst();

                        if(marcador.startsWith("lista_")){
                            //Si se trata de una lista, por cada fila de la SQL añadiremos un parrafo

                            List<IBodyElement> elements = new ArrayList<>();
                            IBodyElement elem = elementosMarcador.get(0);


                            String numFmt = "";
                            if(elem instanceof XWPFParagraph){
                                XWPFParagraph paragraph = (XWPFParagraph) elem;
                                numFmt = paragraph.getNumFmt();
                                while (rs.next()){
                                    elements.add(reemplazarMarcador(paragraph,rs,true));
                                }
                            }
                            copiarElementos(document,elements,numFmt,template);



                        }else if(marcador.startsWith("tabla_")){
                            //Si se trata de una tabla, por cada fila de la SQL añadiremos una fila


                            //Para el caso de _proapp1_, como hay que consultar una base de datos se hace ad-hoc
                            if(marcador.contains("_proapp1_")){
                                String[] m = marcador.split("_proapp1_");
                                if(m.length>1){

                                    String[] parts = servicioBD.obtenerTablaSQL(rs).split("::::");
                                    String tabla = parts[1];
                                    String segundo_marcador = m[1];
                                    String segunda_sql = servicioBD.obtenerSQL(segundo_marcador);

                                    int num_municipios = Integer.parseInt(parts[0]);
                                    int anyos_mostrar = 1;

                                    String final_guery = "select t1.*,t2.* from ("+tabla+") t1, ("+segunda_sql+") t2 where t1.c_muni_ine=t2.codmun limit "+num_municipios*anyos_mostrar;
                                    rs = servicioBD.execSQL_geopub(final_guery);
                                }
                            }

                            for (IBodyElement elem : elementosMarcador) {
                                if (elem instanceof XWPFTable) {
                                    XWPFTable table = (XWPFTable) elem;
                                    procesarTabla(rs,table,document);
                                }
                            }

                        }else if(marcador.equals("normativa")){

                            XWPFParagraph p = document.createParagraph();
                            numParagraphs++;

                            int num=0;
                            while (rs.next()){
                                if(num==0){

                                    copiarElementos(document,elementosMarcador,template);
                                }
                                num++;
                                String codigo= rs.getString("codigo");

                                XWPFParagraph par = document.createParagraph();
                                XWPFRun run = par.createRun();
                                run.setText(codigo);
                                run.setFontFamily("Arial");
                                run.setFontSize(11);
                                numParagraphs++;

                                JsonObject json = new Gson().fromJson(relacion, JsonObject.class);
                                JsonArray array = json.getAsJsonArray(codigo);

                                if(array!=null){
                                    for(int i = 0; i<array.size();i++){
                                        String tema = array.get(i).getAsString();
                                        ResultSet rs2 = servicioBD.execSQL_ideaigar("select distinct descrip from rjt.normativa where tema='"+tema+"' and descrip is not null and id in "+decretosValidos);
                                        while (rs2.next()){
                                            String descrip = rs2.getString("descrip");
                                            insertarLista(document,descrip);
                                        }
                                    }
                                }
                            }
                        }else if(marcador.equals("normativa2")){

                            XWPFParagraph p = document.createParagraph();
                            numParagraphs++;

                            while (rs.next()){
                                String codigo= rs.getString("cespecie");

                                String zona= rs.getString("zona");

                                XWPFParagraph par = document.createParagraph();
                                XWPFRun run = par.createRun();
                                run.setText(zona);
                                run.setFontFamily("Arial");
                                run.setFontSize(11);
                                numParagraphs++;


                                JsonObject json = new Gson().fromJson(relacion, JsonObject.class);
                                JsonArray array = json.getAsJsonArray(codigo);


                                if(array!=null){
                                    for(int i = 0; i<array.size();i++){
                                        String tema = array.get(i).getAsString();
                                        ResultSet rs2 = servicioBD.execSQL_ideaigar("select distinct descrip from rjt.normativa where tema='"+tema+"' AND catego='Régimen de Protección' and id in "+decretosValidos);
                                        while (rs2.next()){
                                            String descrip = rs2.getString("descrip");
                                            insertarLista(document,descrip);
                                        }
                                    }
                                }

                            }

                        }else{
                            //Para el resto de casos se reemplazarán las etiquetas del texto por el primer valor de la SQL

                            if(marcador.contains("_proapp1_")){
                                rs = servicioBD.execSQL_geopub("select distinct substring(max(tempo)::text,0,5) || '/' || substring(max(tempo)::text,5,2) as tempo from geopub.proapp1_iaesmap_map_valores where codmap in ('02M','03M','04M','05M')");
                            }


                            if(rs.next()){
                                List<IBodyElement> elements = new ArrayList<>();
                                for (IBodyElement elem : elementosMarcador) {

                                    if (elem instanceof XWPFParagraph) {
                                        XWPFParagraph paragraph = (XWPFParagraph) elem;
                                        elements.add(reemplazarMarcador(paragraph,rs,false));

                                    } else if (elem instanceof XWPFTable) {
                                        XWPFTable table = (XWPFTable) elem;

                                        for(XWPFTableRow row: table.getRows()){
                                            for(XWPFTableCell cell: row.getTableCells()){
                                                for(XWPFParagraph paragraph: cell.getParagraphs()){
                                                    reemplazarMarcador(paragraph,rs,false);
                                                }
                                            }
                                        }
                                        elements.add(table);
                                    }

                                }
                                log.info(elements.size()+"");
                                copiarElementos(document,elements,template);
                            }
                        }
                    }
                }
            }

        }catch (Exception e){
            util.logException(e);
        }

    }



    public void insertarLista(XWPFDocument document, String texto) throws Exception{

        Random rand = new Random();
        BigInteger id = new BigInteger(32, rand);

        XWPFParagraph p = document.createParagraph();
        numParagraphs++;

        CTNumbering cTNumbering = CTNumbering.Factory.parse(getCTAbstractNumBulletXML(id));
        CTAbstractNum cTAbstractNum = cTNumbering.getAbstractNumArray(0);
        XWPFAbstractNum abstractNum = new XWPFAbstractNum(cTAbstractNum);
        XWPFNumbering numbering = document.createNumbering();
        BigInteger abstractNumID = numbering.addAbstractNum(abstractNum);
        BigInteger numID = numbering.addNum(abstractNumID);
        p.setNumID(numID);
        listas.put(id,numID);

        XWPFRun run = p.createRun();
        run.setText(texto);
        run.setFontFamily("Arial");
        run.setFontSize(11);

    }



    public void copiarElementos(XWPFDocument document,List<IBodyElement> elementosMarcador,XWPFDocument template){
        copiarElementos(document,elementosMarcador,null,template);
    }

    public void copiarElementos(XWPFDocument document,List<IBodyElement> elementosMarcador, String numFmt,XWPFDocument template){
        for (IBodyElement elem : elementosMarcador) {
            if (elem instanceof XWPFParagraph) {

                XWPFParagraph paragraph = (XWPFParagraph) elem;
                copiarParrafo(document,paragraph,numParagraphs, numFmt,template);
                numParagraphs++;
            } else if (elem instanceof XWPFTable) {
                XWPFTable table = (XWPFTable) elem;
                copiarTabla(document,table,numTables);
                numTables++;
            }
        }
    }

    public void procesarTabla(ResultSet rs, XWPFTable table,XWPFDocument document){

        try {
            boolean firstRow = true;



            //Los cambios se hacen sobre una copia de la tabla
            XWPFTable finalTable = null;
            XWPFTableRow headerRow = null;
            XWPFTableRow newRow = null;
            XWPFTableRow row = null;

            while(rs.next()){
                if(firstRow){
                    //Los cambios se hacen sobre una copia de la tabla
                    finalTable = temp.createTable();

                    headerRow = table.getRow(0);
                    newRow = finalTable.createRow();
                    cloneRow(newRow,headerRow);
                    row = table.getRow(1);
                    firstRow = false;
                }
                //Se copia la segunda fila
                newRow = finalTable.createRow();
                cloneRow(newRow,row);


                //Se sustituyen las etiquetas
                for(XWPFTableCell cell: newRow.getTableCells()){
                    for(XWPFParagraph paragraph: cell.getParagraphs()){
                        reemplazarMarcador(paragraph,rs,false);
                    }
                }
            }
            if(!firstRow){
                finalTable.removeRow(0);
                copiarTabla(document,finalTable,numTables);
                numTables++;
            }
        }catch (Exception e){
            util.logException(e);
        }

    }



    public XWPFParagraph reemplazarMarcador(XWPFParagraph par,ResultSet rs, boolean copia) throws SQLException{
        if(copia){
            //Los cambios se hacen sobre una copia del parrafo
            XWPFParagraph paragraph2 = temp.createParagraph();
            cloneParagraph(paragraph2,par);
            //System.out.println(par.getText());
            par = paragraph2;

        }

        for(XWPFRun run: par.getRuns()){

            //Para cada parrafo se sustituye las etiquetas por los valores devueltos por la SQL
            String text = run.getText(run.getTextPosition());
            //log.info(text);
            if(text!=null){
                Pattern pattern = Pattern.compile("@@[^@]*@@", Pattern.CASE_INSENSITIVE);
                Matcher m = pattern.matcher(text);

                while (m.find()) {
                    String etiqueta = m.group(m.groupCount());
                    String value = rs.getString(etiqueta.replaceAll("@@",""));
                    if(value==null || value.equals("null")){
                        value="";
                    }
                    //log.info("reemplazando: " +etiqueta +"  con: "+value);
                    text = text.replaceAll(etiqueta,value);
                }
                run.setText(text,0);
            }
        }
        return par;
    }

    public void copiarParrafo(XWPFDocument document, XWPFParagraph paragraph,int pos, String numFmt, XWPFDocument template){
        //System.out.println("copiarParrafo");
        //System.out.println(numFmt);
        XWPFParagraph p = document.createParagraph();
        //log.info(document.toString());
        //log.info(paragraph.toString());
        //log.info(pos+"");
        document.setParagraph(paragraph,pos);

        if(p.getNumID()!=null){
            XWPFAbstractNum num = template.getNumbering().getAbstractNum(paragraph.getNumID());
            setNumbering(p.getNumID(),p,document,num);
        }
    }


    public void setNumbering(BigInteger id, XWPFParagraph p, XWPFDocument document,XWPFAbstractNum num){
        try{
            XWPFNumbering numbering = document.createNumbering();
            BigInteger abstractNumID = numbering.addAbstractNum(num);
            BigInteger numID = numbering.addNum(abstractNumID);
            p.setNumID(numID);
            listas.put(id,numID);

        }catch (Exception e){
            util.logException(e);
        }
    }

    public void copiarTabla(XWPFDocument document, XWPFTable table,int pos){
        document.createTable();
        log.info("COPIADA TABLA EN : "+pos);
        document.setTable(pos,table);
    }



    public static byte[] hexToBytes(String hexString) {
        HexBinaryAdapter adapter = new HexBinaryAdapter();
        byte[] bytes = adapter.unmarshal(hexString);
        return bytes;
    }


    static XWPFHyperlinkRun createHyperlinkRun(XWPFParagraph paragraph, String uri) {
        String rId = paragraph.getDocument().getPackagePart().addExternalRelationship(
                uri,
                XWPFRelation.HYPERLINK.getRelation()
        ).getId();

        CTHyperlink cthyperLink=paragraph.getCTP().addNewHyperlink();
        cthyperLink.setId(rId);
        cthyperLink.addNewR();

        return new XWPFHyperlinkRun(
                cthyperLink,
                cthyperLink.getRArray(0),
                paragraph
        );
    }



    private void cloneTable(XWPFTable target,XWPFTable source) {
        target.getCTTbl().setTblPr(source.getCTTbl().getTblPr());
        target.getCTTbl().setTblGrid(source.getCTTbl().getTblGrid());
        for (int r = 0; r<source.getRows().size(); r++) {
            XWPFTableRow targetRow = target.createRow();
            XWPFTableRow row = source.getRows().get(r);
            cloneRow(targetRow,row);

        }
        //newly created table has one row by default. we need to remove the default row.
        target.removeRow(0);
    }


    public void cloneRow(XWPFTableRow clone, XWPFTableRow source){
        source.getCtRow().getTrPr();
        clone.getCtRow().setTrPr(source.getCtRow().getTrPr());
        for (int c=0; c<source.getTableCells().size(); c++) {
            //newly created row has 1 cell
            XWPFTableCell targetCell = c==0 ? clone.getTableCells().get(0) : clone.createCell();
            XWPFTableCell cell = source.getTableCells().get(c);
            targetCell.getCTTc().setTcPr(cell.getCTTc().getTcPr());
            XmlCursor cursor = targetCell.getParagraphArray(0).getCTP().newCursor();
            for (int p = 0; p < cell.getBodyElements().size(); p++) {
                IBodyElement elem = cell.getBodyElements().get(p);
                if (elem instanceof XWPFParagraph) {
                    XWPFParagraph targetPar = targetCell.insertNewParagraph(cursor);
                    cursor.toNextToken();
                    XWPFParagraph par = (XWPFParagraph) elem;
                    cloneParagraph(targetPar,par);
                } else if (elem instanceof XWPFTable) {
                    XWPFTable targetTable = targetCell.insertNewTbl(cursor);
                    XWPFTable table = (XWPFTable) elem;
                    cloneTable(targetTable,table);
                    cursor.toNextToken();
                }
            }
            //newly created cell has one default paragraph we need to remove
            targetCell.removeParagraph(targetCell.getParagraphs().size()-1);
        }
    }



    public void cloneParagraph(XWPFParagraph clone, XWPFParagraph source) {
        CTPPr pPr = clone.getCTP().isSetPPr() ? clone.getCTP().getPPr() : clone.getCTP().addNewPPr();
        pPr.set(source.getCTP().getPPr());

        for (XWPFRun r : source.getRuns()) {
            XWPFRun nr = clone.createRun();
            cloneRun(nr, r);
        }
    }

    public void cloneRun(XWPFRun clone, XWPFRun source) {
        CTRPr rPr = clone.getCTR().isSetRPr() ? clone.getCTR().getRPr() : clone.getCTR().addNewRPr();
        rPr.set(source.getCTR().getRPr());
        clone.setText(source.getText(0));
    }




    public String getCTAbstractNumBulletXML(BigInteger id){
        return "<w:abstractNum xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" w:abstractNumId=\""+id+"\">"
                + "<w:multiLevelType w:val=\"hybridMultilevel\"/>"
                + "<w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:numFmt w:val=\"bullet\"/><w:lvlText w:val=\"\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Symbol\" w:hAnsi=\"Symbol\" w:hint=\"default\"/></w:rPr></w:lvl>"
                + "<w:lvl w:ilvl=\"1\" w:tentative=\"1\"><w:start w:val=\"1\"/><w:numFmt w:val=\"bullet\"/><w:lvlText w:val=\"o\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Courier New\" w:hAnsi=\"Courier New\" w:cs=\"Courier New\" w:hint=\"default\"/></w:rPr></w:lvl>"
                + "<w:lvl w:ilvl=\"2\" w:tentative=\"1\"><w:start w:val=\"1\"/><w:numFmt w:val=\"bullet\"/><w:lvlText w:val=\"\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"360\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Wingdings\" w:hAnsi=\"Wingdings\" w:hint=\"default\"/></w:rPr></w:lvl>"
                + "</w:abstractNum>";
    }

    public String getCTAbstractNumDecimalXML(BigInteger id){
        return "<w:abstractNum xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" w:abstractNumId=\""+id+"\">"
                + "<w:multiLevelType w:val=\"hybridMultilevel\"/>"
                + "<w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:numFmt w:val=\"decimal\"/><w:lvlText w:val=\"%1\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"0\" w:hanging=\"360\"/></w:pPr></w:lvl>"
                + "<w:lvl w:ilvl=\"1\" w:tentative=\"1\"><w:start w:val=\"1\"/><w:numFmt w:val=\"decimal\"/><w:lvlText w:val=\"%1.%2\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr></w:lvl>"
                + "<w:lvl w:ilvl=\"2\" w:tentative=\"1\"><w:start w:val=\"1\"/><w:numFmt w:val=\"decimal\"/><w:lvlText w:val=\"%1.%2.%3\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"360\"/></w:pPr></w:lvl>"
                + "</w:abstractNum>";
    }





}
