
package idearium.otinfows.servicios;

import idearium.otinfows.RestController;
import idearium.otinfows.modelo.Expediente;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.geotools.geopkg.GeoPackage;

@Service
public class ServicioAsincrono {


    private final static Logger log = LoggerFactory.getLogger(ServicioAsincrono.class.getName());

    @Autowired
    Util util;

    @Autowired
    ServicioBD servicioBD;

    @Autowired
    ServicioCalculosEspaciales servicioCalculosEspaciales;

    @Autowired
    ServicioFTP servicioFTP;

    @Autowired
    ServicioWord servicioWord;

    @Autowired
    ServicioGeopackage servicioGeopackage;

    @Value("${informe}")
    public String informe;

    @Value("${temp.path}")
    String tempPath;



    @Async("threadPoolTaskExecutor")
    public void iniciarProceso(Expediente exp, boolean haySHP, String request) {

        try{
        	 Util.log.debug("iniciar proceso "+haySHP);
          if (haySHP){
        	  Util.log.debug("haySHP "+request);
            if (request=="UPDATE"){
                servicioBD.borrarAfecciones(exp.getCod_sct());
            }
            
            try{
            servicioCalculosEspaciales.realizarCalculos(exp.getCod_sct());
            }
            catch(Exception ex){
            	util.logException(ex);
            	servicioBD.actualizarEstado(exp.getCod_sct(),-1,ex.getClass().getCanonicalName()+": "+ex.getMessage());
            	return;
            }
          }
          String ruta_informe=null;
          try{
            ruta_informe = servicioWord.generarInforme(exp.getCod_sct());
           
          }
          catch(Exception ex){
          	util.logException(ex);
          
          	servicioBD.actualizarEstado(exp.getCod_sct(),-2,ex.getClass().getCanonicalName()+": "+ex.getMessage());
          	return;
          }
         
          try{
            InputStream in = new FileInputStream(ruta_informe);
            
            String  url = servicioFTP.subirFichero(in,exp.getCod_sct(),".docx");
            servicioBD.actualizarUrlWord(exp.getCod_sct(),url);
            //Una vez subido se borra de la carpta temporal
            new File(ruta_informe).delete();
          }
          catch(Exception ex){
            	util.logException(ex);
            	servicioBD.actualizarEstado(exp.getCod_sct(),-3,ex.getClass().getCanonicalName()+": "+ex.getMessage());
            	return;
            }
          String ruta_geopackage=null;
          try{
        	  ruta_geopackage = servicioGeopackage.generarGeopackage(exp.getCod_sct());
          }
          catch(Exception ex){
          	util.logException(ex);
          
          	servicioBD.actualizarEstado(exp.getCod_sct(),-5,ex.getClass().getCanonicalName()+": "+ex.getMessage());
          	return;
          }
          
          try{
        	  InputStream in2 = new FileInputStream(ruta_geopackage);
        	  String url = servicioFTP.subirFichero(in2,exp.getCod_sct(),".gpkg");
              //Una vez subido se borra de la carpta temporal
              new File(ruta_geopackage).delete();
           
           servicioBD.actualizarUrlGeopackage(exp.getCod_sct(),url);
          }
          catch(Exception ex){
            	util.logException(ex);
            	servicioBD.actualizarEstado(exp.getCod_sct(),-6,ex.getClass().getCanonicalName()+": "+ex.getMessage());
            	return;
            }
          
                servicioBD.actualizarEstado(exp.getCod_sct(),1);
                
            


            log.info("------------------------------------------------------------------");
            log.info("---------------------------FINALIZADO-----------------------------");
            log.info("------------------------------------------------------------------");
        }catch (Exception e){
            util.logException(e);
        }

    }
    @Async("threadPoolTaskExecutor")
    public void iniciarProceso(String exp, int estado) {
    	try{
    	if (estado==-1){
    		   try{
    			    servicioBD.borrarAfecciones(exp);
    	            servicioCalculosEspaciales.realizarCalculos(exp);
    	            }
    	            catch(Exception ex){
    	            	util.logException(ex);
    	            	servicioBD.actualizarEstado(exp,-1,ex.getClass().getCanonicalName()+": "+ex.getMessage());
    	            	return;
    	            }
    	}
    	if (estado>=-2){
    		String ruta_informe=null;
          try{
        	  ruta_informe=servicioWord.generarInforme(exp);
          }
          catch(Exception ex){
            util.logException(ex);
          	servicioBD.actualizarEstado(exp,-2,ex.getClass().getCanonicalName()+": "+ex.getMessage());
          	return;
          }
          
          String url;
          try{
              InputStream in = new FileInputStream(ruta_informe);
              url = servicioFTP.subirFichero(in,exp,".docx");
              
          }
          catch(Exception ex){
            	util.logException(ex);
            	servicioBD.actualizarEstado(exp,-3,ex.getClass().getCanonicalName()+": "+ex.getMessage());
            	return;
            }
    	
          servicioBD.actualizarUrlWord(exp,url);
                servicioBD.actualizarUrlWord(exp,url);
    	}    
    	if (estado>=-5){
    		 String ruta_geopackage=null;
             try{
           	  ruta_geopackage = servicioGeopackage.generarGeopackage(exp);
             }
             catch(Exception ex){
             	util.logException(ex);
             
             	servicioBD.actualizarEstado(exp,-5,ex.getClass().getCanonicalName()+": "+ex.getMessage());
             	return;
             }
             String url ;
             try{
           	  InputStream in2 = new FileInputStream(ruta_geopackage);
           	  url = servicioFTP.subirFichero(in2,exp,".gpkg");

              
             
             }
             catch(Exception ex){
               	util.logException(ex);
               	servicioBD.actualizarEstado(exp,-6,ex.getClass().getCanonicalName()+": "+ex.getMessage());
               	return;
               }
           
             servicioBD.actualizarUrlGeopackage(exp,url);

              
    	}   
    	 servicioBD.actualizarEstado(exp,1);
            log.info("------------------------------------------------------------------");
            log.info("---------------------------FINALIZADO-----------------------------");
            log.info("------------------------------------------------------------------");
        }catch (Exception e){
            util.logException(e);
        }

    }

}
