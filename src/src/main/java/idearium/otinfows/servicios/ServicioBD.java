package idearium.otinfows.servicios;

import java.io.File;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import com.google.gson.JsonArray;
import com.vividsolutions.jts.geom.Geometry;
import org.geotools.data.*;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.geopkg.FeatureEntry;
import org.geotools.geopkg.GeoPackage;
import org.opengis.feature.simple.SimpleFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.JsonObject;

import idearium.otinfows.modelo.Afeccion;
import idearium.otinfows.modelo.CapaAfeccion;
import idearium.otinfows.modelo.DominioListaValores;
import idearium.otinfows.modelo.Expediente;
import idearium.otinfows.modelo.ListaValores;
import idearium.otinfows.modelo.Novedad;

@Service
public class ServicioBD {

    @Autowired
    DataSource dataSource;

    @Value("${temp.path}")
    String tempPath;

    @Value("${codelists}")
    public String codelists;
    
    String SHP_ENCODING="ISO-8859-1";

    Connection c;

    Connection c_geopub;

    Connection c_ideaigar;


    @Value("${url_geopub}")
    public String url_geopub;
    @Value("${user_geopub}")
    public String user_geopub;
    @Value("${password_geopub}")
    public String password_geopub;



    @Value("${url_ideaigar}")
    public String url_ideaigar;
    @Value("${user_ideaigar}")
    public String user_ideaigar;
    @Value("${password_ideaigar}")
    public String password_ideaigar;


    @Autowired
    Util util;

    private final static Logger log = LoggerFactory.getLogger(ServicioBD.class.getName());

    @PostConstruct
    public void  inicialize() throws SQLException {
        c = dataSource.getConnection();
        c_ideaigar = DataSourceBuilder.create()
                .driverClassName("org.postgresql.Driver")
                .username(user_ideaigar)
                .password(password_ideaigar)
                .url(url_ideaigar)
                .build().getConnection();
        c_geopub = DataSourceBuilder.create()
                .driverClassName("org.postgresql.Driver")
                .username(user_geopub)
                .password(password_geopub)
                .url(url_geopub)
                .build().getConnection();
    }


    public void insertarExpediente(Expediente exp) throws Exception{
       
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));

             c.createStatement().execute("INSERT INTO eota.t999_otinfo_exp (title, cod_sct, date_ini," +
             "cota,solicitante, exp_solicitud, promotor, type_proc, type_proy, solicitante_txt, fecha_mod, estado,fecha_estado,solicitante_txt_con_articulo,solicitante_subnivel,caducado,fecha_caducidad)\n" +
             "VALUES ('"+exp.getTitle()+"', '"+exp.getCod_sct()+"', '"+exp.getDate_ini()+"',"+exp.getCota()+
             ","+exp.getSolicitante()+",'"+exp.getExp_solicitud()+"','"+exp.getPromotor()+"','"+exp.getType_proc()+
             "','"+exp.getType_proy()+"','"+exp.getSolicitante_txt()+"','"+sdf.format(new Date())+"',0,current_timestamp,'"+exp.getSolicitante_txt_con_articulo()+"','"+exp.getSolicitante_subnivel()+"',"+
             (exp.getCaducado()!=null && exp.getCaducado()==true ? "true":"false")+","+(exp.getCaducado()!=null && exp.getCaducado()==true?"'"+exp.getFecha_caducidad()+"'":"null")+")");

       
    }
    public void actualizarExpediente(Expediente exp) throws Exception{
    	
    		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    		sdf.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
    		Util.log.debug("UPDATE eota.t999_otinfo_exp set title='"+exp.getTitle()+"', date_ini='"+exp.getDate_ini()+"'," +
    				"cota="+exp.getCota()+",solicitante="+exp.getSolicitante()+", exp_solicitud='"+exp.getExp_solicitud()+"', promotor='"+exp.getPromotor()+
    				"', type_proc='"+ exp.getType_proc()+"', type_proy="+ exp.getType_proy()+", solicitante_txt='"+exp.getSolicitante_txt()+"', solicitante_subnivel='"+exp.getSolicitante_subnivel()+"', fecha_mod='"+sdf.format(new Date())+
    				"', estado=0,fecha_estado=current_timestamp, solicitante_txt_con_articulo='"+exp.getSolicitante_txt_con_articulo()+"', caducado="+(exp.getCaducado()!=null && exp.getCaducado()==true ?"true":"false")+",fecha_caducidad="+(exp.getCaducado()!=null && exp.getCaducado()==true?"'"+exp.getFecha_caducidad()+"'":"null")+" where cod_sct='"+exp.getCod_sct()+"'");
    		int result = c.createStatement().executeUpdate("UPDATE eota.t999_otinfo_exp set title='"+exp.getTitle()+"', date_ini='"+exp.getDate_ini()+"'," +
    				"cota="+exp.getCota()+",solicitante="+exp.getSolicitante()+", exp_solicitud='"+exp.getExp_solicitud()+"', promotor='"+exp.getPromotor()+
    				"', type_proc='"+ exp.getType_proc()+"', type_proy="+ exp.getType_proy()+", solicitante_txt='"+exp.getSolicitante_txt()+"', solicitante_subnivel='"+exp.getSolicitante_subnivel()+"', fecha_mod='"+sdf.format(new Date())+
    				"', estado=0,fecha_estado=current_timestamp, solicitante_txt_con_articulo='"+exp.getSolicitante_txt_con_articulo()+"', caducado="+exp.getCaducado()+",fecha_caducidad="+(exp.getCaducado()?"'"+exp.getFecha_caducidad()+"'":"null")+" where cod_sct='"+exp.getCod_sct()+"'");
    		if (result<=0){
    			throw new Exception("El expediente no existe:"+exp.getCod_sct());
    		}
    	
    }
    public void borrarExpediente(String cod_sct) throws Exception{
    	borrarAfecciones(cod_sct);
    	
    		Util.log.debug("DELETE FROM eota.t999_otinfo_exp  where cod_sct='"+cod_sct+"'");
    	Statement stmt = c.createStatement();
    	stmt.execute("DELETE FROM eota.t999_otinfo_exp_errores  where cod_sct='"+cod_sct+"'");
    	stmt.execute("DELETE FROM eota.t999_otinfo_exp_warnings  where cod_sct='"+cod_sct+"'");
    		stmt.execute("DELETE FROM eota.t999_otinfo_exp  where cod_sct='"+cod_sct+"'");
    }
    public Expediente obtenerSolicitanteTxt(Expediente expediente){
        try{
            ResultSet rs = c.createStatement().executeQuery("select * from eota.t999_otinfo_solicitante where id="+expediente.getSolicitante());
            if(rs.next()){
                String lista_subnivel = rs.getString("lista_subnivel");
                if(lista_subnivel==null){
                    expediente.setSolicitante_txt(rs.getString("nombre"));
                    expediente.setSolicitante_txt_con_articulo(rs.getString("nombre_con_articulo"));
                }else{
                    expediente.setSolicitante_txt(rs.getString("nombre")+" de "+expediente.getSolicitante_subnivel());
                    expediente.setSolicitante_txt_con_articulo(rs.getString("nombre_con_articulo")+" "+expediente.getSolicitante_subnivel());
                }
                return expediente;
            }else{
                return expediente;
            }
        }catch (Exception e){
            util.logException(e);
            return expediente;
        }

    }

    public Integer getEstado(String cod_sct) throws Exception{
    	Statement stmt = c.createStatement();
    	Integer estado;
    	try{
        	
            ResultSet rs=stmt.executeQuery("select estado from eota.t999_otinfo_exp  where cod_sct='"+cod_sct+"'");
            if (rs.next()){
            	estado = rs.getInt("estado");
            	if (rs.wasNull()){
            		estado=null;
            	}
            	
            }
            else{
            	throw new Exception("El expediente no existe:"+cod_sct);
            }
            
        }finally{
            stmt.close();
        }
    	return estado;
    }
    public void actualizarEstado(String cod_sct,int estado){
    	actualizarEstado(cod_sct,estado,null);
    }
    public void actualizarEstado(String cod_sct,int estado, String error){
    	util.log.debug("actualizar estado "+(error !=null ? error:""));
        try{
        	Statement stmt = c.createStatement();
            stmt.execute("update eota.t999_otinfo_exp set estado="+estado+",fecha_estado=current_timestamp where cod_sct='"+cod_sct+"'");
            if (estado>=0){
            	stmt.execute("delete from eota.t999_otinfo_exp_errores where cod_sct='"+cod_sct+"'");
            }
            else if (error != null){
            	int resultado = stmt.executeUpdate("update eota.t999_otinfo_exp_errores set error='"+error+"' where cod_sct='"+cod_sct+"'");
            	if (resultado<=0){
            		stmt.executeUpdate("INSERT INTO eota.t999_otinfo_exp_errores(cod_sct,error) values ('"+cod_sct+"','"+error+"')");
            	}
            }
        }catch (Exception e){
            util.logException(e);
        }
    }
    public void guardarWarning(String cod_sct, String error){

    	try{
    		Statement stmt = c.createStatement();
    		Util.log.debug("update eota.t999_otinfo_exp_warnings set error=replace(error,'</ul>','')||'"+error+"' where cod_sct='"+cod_sct+"'");
    		int resultado = stmt.executeUpdate("update eota.t999_otinfo_exp_warnings set error=replace(error,'</ul>','')||'"+error+"' where cod_sct='"+cod_sct+"'");
    		if (resultado<=0){
    			stmt.executeUpdate("INSERT INTO eota.t999_otinfo_exp_warnings(cod_sct,error) values ('"+cod_sct+"','"+error+"')");

    		}
    	}catch (Exception e){
    		util.logException(e);
    	}
    }
    public void actualizarUrlWord(String cod_sct, String url){
    	try{
    	Statement stmt =c.createStatement(); 
        stmt.execute("update eota.t999_otinfo_exp set url_word='"+url+"' where cod_sct='"+cod_sct+"'");
        stmt.close();
    	}
    	catch(Exception ex){
    		Util.log.error("Error actualizando url Word: "+"update eota.t999_otinfo_exp set url_word='"+url+"' where cod_sct='"+cod_sct+"'");
    		util.logException(ex);
    	}
    }

    public void actualizarUrlGeopackage(String cod_sct, String url) {
    try{
    	Statement stmt =c.createStatement();
    	stmt.execute("update eota.t999_otinfo_exp set url_geopackage='"+url+"' where cod_sct='"+cod_sct+"'");
         stmt.close();
    }
	catch(Exception ex){
		Util.log.error("Error actualizando url Geopoackage: "+"update eota.t999_otinfo_exp set url_geopackage='"+url+"' where cod_sct='"+cod_sct+"'");
		util.logException(ex);
	}
    }
    public void actualizarUrlPDF(String cod_sct,String url){
    	try{
    	Statement stmt =c.createStatement();
    	stmt.execute("update eota.t999_otinfo_exp set url_pdf='"+url+"' where cod_sct='"+cod_sct+"'");
         stmt.close();
    }
 	catch(Exception ex){
 		Util.log.error("Error actualizando url PDF: "+"update eota.t999_otinfo_exp set url_pdf='"+url+"' where cod_sct='"+cod_sct+"'");
 		util.logException(ex);
 	}
    }

    public CapaAfeccion[] getAfecciones(String cod_sct) throws Exception{
    	
            Util.log.debug("select c.* from eota.otinfo_afecciones_capas c inner join eota.otinfo_afecciones a on c.tabla=a.tabla_afecciones  WHERE a.cod_sct='" +cod_sct+"'");

            ResultSet rs = c.createStatement().executeQuery("select c.* from eota.otinfo_afecciones_capas c inner join eota.otinfo_afecciones a on c.tabla=a.tabla_afecciones  WHERE a.cod_sct='" +cod_sct+"' order by c.titulo");
            CapaAfeccion[] capas= CapaAfeccion.extraer(rs);
            Statement stmt = c.createStatement();
            for (CapaAfeccion capa:capas){
            	
                Util.log.debug("select objectid, "+capa.getCampo_titulo()+" as titulo,area_influencia from eota."+capa.getTabla()+" where cod_sct='"+cod_sct+"' order by area_influencia DESC");
                try{
            	rs = stmt.executeQuery("select objectid, "+capa.getCampo_titulo()+" as titulo,area_influencia from eota."+capa.getTabla()+" where cod_sct='"+cod_sct+"' order by area_influencia DESC");
            	}
                catch(SQLException ex){
                	Util.log.error("Error al consultar afecciones de "+capa.getTabla(), ex);
                	rs = stmt.executeQuery("select null as objectid, "+capa.getCampo_titulo()+" as titulo,area_influencia from eota."+capa.getTabla()+" where cod_sct='"+cod_sct+"' order by area_influencia DESC");
                }
            	 Afeccion[] afecciones = Afeccion.extraer(rs);
            	 capa.setAfecciones(afecciones);
            }
            return capas;
       
    }
    
    public Novedad[] getNovedades(Long timestamp) throws Exception{
    	
        Util.log.debug("select e.cod_sct,e.estado,err.error from eota.t999_otinfo_exp e left join eota.t999_otinfo_exp_errores err on  e.cod_sct=err.cod_sct where fecha_estado>to_timestamp("+timestamp+"/1000)");

        ResultSet rs = c.createStatement().executeQuery("select e.cod_sct,e.estado,err.error from eota.t999_otinfo_exp e left join eota.t999_otinfo_exp_errores err on  e.cod_sct=err.cod_sct where fecha_estado>to_timestamp("+timestamp+"/1000)");
        Novedad[] novedades= Novedad.extraer(rs);
        
        rs.getStatement().close();
       
        return novedades;
   
}
    public ListaValores[] getCodeLists() throws Exception{
    	
    	String[] listas = codelists.split(",");
    	 Statement stmt = c.createStatement();
    	 ListaValores[] listas_valores = new ListaValores[listas.length];
    	for (int i=0; i<listas.length;i++ ){
    		ListaValores lista = new ListaValores();
    		lista.setNombre(listas[i]);
    		Util.log.debug("select id,nombre"+(listas[i].equalsIgnoreCase("t999_otinfo_solicitante")?",lista_subnivel  as complemento":"")+" from eota."+listas[i]+" order by id");
        	 ResultSet rs = stmt.executeQuery("select id,nombre"+(listas[i].equalsIgnoreCase("t999_otinfo_solicitante")?",lista_subnivel as complemento":"")+" from eota."+listas[i]+" order by id");
        	 DominioListaValores[] valores = DominioListaValores.extraer(rs);
        	 lista.setValores(valores);
        	 listas_valores[i]=lista;
        }
        return listas_valores;
   
}
    public void importarShp(Expediente exp,MultipartFile DBF_file, MultipartFile SHX_file, MultipartFile SHP_file, MultipartFile PROJ_file) throws Exception{

            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));

            File file = new File(tempPath+exp.getCod_sct()+"_"+sdf.format(new Date())+".dbf");
            DBF_file.transferTo(file);

            file = new File(tempPath+exp.getCod_sct()+"_"+sdf.format(new Date())+".shx");
            SHX_file.transferTo(file);

            if(PROJ_file!=null){
                file = new File(tempPath+exp.getCod_sct()+"_"+sdf.format(new Date())+".prj");
                PROJ_file.transferTo(file);
            }

            file = new File(tempPath+exp.getCod_sct()+"_"+sdf.format(new Date())+".shp");
            SHP_file.transferTo(file);

            FileDataStore store = FileDataStoreFinder.getDataStore(file);


            ((ShapefileDataStore)store).setCharset(Charset.forName(SHP_ENCODING));

            SimpleFeatureSource featureSource = store.getFeatureSource();
            SimpleFeatureIterator features =  featureSource.getFeatures().features();

            int i = 0;
            while(features.hasNext()){
                SimpleFeature feature = features.next();
                Geometry geom = (Geometry)feature.getDefaultGeometry();
                if(i==0){
                    c.createStatement().execute("DELETE FROM eota.t999_otinfo_cod_sct where cod_sct='"+exp.getCod_sct()+"'");
                }
                 c.createStatement().execute("INSERT INTO eota.t999_otinfo_cod_sct (cod_sct, shape)\n" +
                 "VALUES ('"+exp.getCod_sct()+"', ST_GeomFromText('"+geom.toText()+"','"+25830+"'));");
                 i++;

            }

            //Una vez importados, se borran de la carpeta temporal
            file = new File(tempPath+exp.getCod_sct()+"_"+sdf.format(new Date())+".dbf");
            file.delete();
            file = new File(tempPath+exp.getCod_sct()+"_"+sdf.format(new Date())+".shx");
            file.delete();
            if(PROJ_file!=null){
                file = new File(tempPath+exp.getCod_sct()+"_"+sdf.format(new Date())+".prj");
                file.delete();
            }
            file = new File(tempPath+exp.getCod_sct()+"_"+sdf.format(new Date())+".shp");
            file.delete();


            features.close();
    }


    public String obtenerBoundingBox(String cod_sct, String area_influencia){
        try{
            String q = "";

            if(area_influencia.equals("10k")){
                q = "select ST_Extent(ST_Buffer(shape,10000)) as bbox from eota.t999_otinfo_cod_sct where cod_sct ='"+cod_sct+"'";
            }else if(area_influencia.equals("1k")){
                q = "select ST_Extent(ST_Buffer(shape,1000)) as bbox from eota.t999_otinfo_cod_sct where cod_sct ='"+cod_sct+"'";
            }else{
                q = "select ST_Extent(shape) as bbox from eota.t999_otinfo_cod_sct where cod_sct ='"+cod_sct+"'";
            }

            ResultSet rs = c.createStatement().executeQuery(q);
            if(rs.next()){
                String bbox = rs.getString("bbox");
                bbox = bbox.replaceAll("BOX\\(","");
                bbox = bbox.replaceAll("\\)","");
                bbox = bbox.replaceAll(" ",",");
                return bbox;
            }else{
                return null;
            }
        }catch (Exception e){
            util.logException(e);
            return null;
        }
    }

    public String obtenerBufferImagen(String marcador){
        try{
            ResultSet rs = c.createStatement().executeQuery("select distinct area_influencia from eota.otinfo_mapas where marcador ='"+marcador+"'");
            if(rs.next()){
                String area_influencia = rs.getString("area_influencia");
                return area_influencia;
            }else{
                return null;
            }
        }catch (Exception e){
            util.logException(e);
            return null;
        }

    }


    public String obtenerSQL(String marcador){
        try{
            ResultSet rs = c.createStatement().executeQuery("select sql from eota.otinfo_sql where marcador ='"+marcador+"'");
            if(rs.next()){
                String sql = rs.getString("sql");
                return sql;
            }else{
                return null;
            }
        }catch (Exception e){
            util.logException(e);
            return null;
        }
    }

    public ResultSet obtenerMapa(String marcador){
        try{
            ResultSet rs = c.createStatement().executeQuery("select * from eota.otinfo_mapas where marcador ='"+marcador+"'  order by orden asc");
            return rs;
        }catch (Exception e){
            util.logException(e);
            return null;
        }
    }


    public boolean afecta(String tabla, String area_influencia, String cod_sct){
        try{
            if(tabla.equals("OTINFO:v999_otinfo_exp") || tabla.equals("OTINFO:v999_otinfo_cod_sct_area_influ")){
                return true;
            }else{
                ResultSet rs =  c.prepareStatement("select * from eota.\""+tabla+"\" where cod_sct='"+cod_sct+"' AND area_influencia='"+area_influencia+"' limit 1",ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery();
                if(rs.next()){
                    return true;
                }else{
                    return false;
                }
            }
        }catch (Exception e){
            util.logException(e);
            return false;
        }
    }



    public ResultSet execSQL(String query){
        try{
            log.info(query);
            ResultSet rs =  c.prepareStatement(query,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery();

            return rs;
        }catch (Exception e){
            util.logException(e);
            return null;
        }
    }
    public ResultSet execSQL_geopub(String query){
        try{
            //log.info(query);
            ResultSet rs =  c_geopub.prepareStatement(query,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery();

            return rs;
        }catch (Exception e){
            util.logException(e);
            return null;
        }
    }

    public ResultSet execSQL_ideaigar(String query){
        try{
            ResultSet rs =  c_ideaigar.prepareStatement(query,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery();

            return rs;
        }catch (Exception e){
            util.logException(e);
            return null;
        }
    }



    public ResultSet obtenerGeometrias(Expediente exp){
        try{
            ResultSet rs = c.createStatement().executeQuery("select * from eota.t999_otinfo_cod_sct where cod_sct ='"+exp.getCod_sct()+"'");
            return  rs;
        }catch (Exception e){
            util.logException(e);
            return null;
        }
    }

    public void borrarAfecciones(String cod_sct) throws SQLException{
    	Statement stmt =null;
    	ResultSet rs =null;
        try{
            rs = c.createStatement().executeQuery("select * from eota.otinfo_afecciones where cod_sct ='"+cod_sct+"'");
            stmt = c.createStatement();
            while (rs.next()){
            	try{
            	stmt.executeUpdate("DELETE FROM eota.\""+rs.getString("tabla_afecciones")+"\" WHERE cod_sct='"+cod_sct+"'");
            	}
            	catch(Exception ex){
            		util.logWarnException(ex);
            	}
            }
            stmt.executeUpdate("DELETE FROM  eota.otinfo_afecciones where cod_sct ='"+cod_sct+"'");
            Util.log.debug("DELETE FROM  eota.t999_otinfo_exp_warnings where cod_sct ='"+cod_sct+"'");
            stmt.executeUpdate("DELETE FROM  eota.t999_otinfo_exp_warnings where cod_sct ='"+cod_sct+"'");
        }catch (Exception e){
            util.logException(e);
            
        }
        finally{
        	rs.getStatement().close();
            rs.close();
            stmt.close();
        }
    }



    public void guardarAfeccion(String cod_sct, String capa){
        try{
            c.createStatement().execute("INSERT INTO eota.otinfo_afecciones (cod_sct,tabla_afecciones) VALUES ('"+cod_sct+"','"+capa+"');");
        }catch (Exception e){
            //util.logException(e);
        }

    }


    public String obtenerTablaSQL(ResultSet rs) throws Exception{

        int num_filas = 0;
        String tabla = "select";
        int num_col = 0;
        if(rs.next()){
            num_filas++;
            ResultSetMetaData rsmetadata = rs.getMetaData();
            num_col = rsmetadata.getColumnCount();
            for(int i = 1; i<=num_col;i++){
                String column_name = rsmetadata.getColumnName(i);
                tabla += " '"+rs.getString(i) +"'::text as "+column_name+",";
            }
            //Quitar la coma del final
            tabla = tabla.substring(0,tabla.length()-1);

        }

        while (rs.next()){
            num_filas++;
            tabla+= " UNION ALL ";
            tabla+= "select ";
            for(int i = 1; i<=num_col;i++){
                tabla += " '"+rs.getString(i)+"',";
            }
            tabla = tabla.substring(0,tabla.length()-1);
        }
        return num_filas+"::::"+tabla;
    }


    public void crearTabla(String capa, JsonArray array) throws Exception{
            String[] parts = capa.split("\\.",2);
            String tabla = parts[1];
            tabla = tabla.replaceAll("\\\"","");
            String columns = "(cod_sct varchar(255), capa varchar(255), area_influencia varchar(255), ";
            for(int i =0; i<array.size(); i++){
                JsonObject field = array.get(i).getAsJsonObject();
                if(field.get("name").getAsString().equals("shape_json")){
                    columns+= "shape geometry,";
                }else if(field.get("name").getAsString().equals("intersection_area")){
                    columns += "area_afeccion"+" "+field.get("type").getAsString()+",";
                }else{
                    columns += "\""+field.get("name").getAsString()+"\""+" "+field.get("type").getAsString()+",";
                }

            }

            String comulmsWithObjectid = columns + " objectid serial)";
            try{
                //Primero probamos a crearla con el serial

                log.info("CREATE TABLE IF NOT EXISTS eota.\"t999_otinfo_geodata_"+tabla+"\" "+comulmsWithObjectid+";");
                c.createStatement().execute("CREATE TABLE IF NOT EXISTS eota.\"t999_otinfo_geodata_"+tabla+"\" "+comulmsWithObjectid+";");
            }catch (Exception e){
                //Ya existe el objectid, por lo que no hace falta poner el serial
                columns = columns.substring(0,columns.length()-1)+")";
                //log.info("CREATE TABLE IF NOT EXISTS eota.\"t999_otinfo_geodata_"+tabla+"\" "+columns+";");
                c.createStatement().execute("CREATE TABLE IF NOT EXISTS eota.\"t999_otinfo_geodata_"+tabla+"\" "+columns+";");
            }

    }

    public void borrarTabla(String capa){
        String[] parts = capa.split("\\.",2);
        String tabla = parts[1];
        tabla = tabla.replaceAll("\\\"","");

        try{
            c.createStatement().execute("drop view eota.\"v999_otinfo_geodata_gpkg_"+tabla+"\"");
        }catch (Exception e){
            util.logException(e);
        }

        try{
            c.createStatement().execute("drop table eota.\"t999_otinfo_geodata_"+tabla+"\"");
        }catch (Exception e){
            util.logException(e);
        }

    }

    public void crearVista(String capa, JsonArray array){
        try{

            String[] parts = capa.split("\\.",2);
            String tabla = parts[1];
            tabla = tabla.replaceAll("\\\"","");
            String columns = "cod_sct::text , capa::text, area_influencia::text, ";
            for(int i =0; i<array.size(); i++){
                JsonObject field = array.get(i).getAsJsonObject();
                if(field.get("name").getAsString().equals("shape_json")){
                    columns+= "shape::geometry,";
                }else{
                    columns += "\""+field.get("name").getAsString()+"\""+"::text,";
                }
            }

            columns = columns.substring(0,columns.length()-1);


            log.info("CREATE VIEW eota.\"v999_otinfo_geodata_gpkg_"+tabla+"\""+" AS SELECT "+columns+" from eota.\"t999_otinfo_geodata_"+tabla+"\" "+";");
            c.createStatement().execute("CREATE VIEW eota.\"v999_otinfo_geodata_gpkg_"+tabla+"\""+" AS SELECT "+columns+" from eota.\"t999_otinfo_geodata_"+tabla+"\" "+";");

        }catch (Exception e){
        }

    }

    public void insertFeature(JsonObject properties, String cod_sct, String capa, String area_influencia, String tabla) throws Exception{

            String columns = "(cod_sct, capa, area_influencia, ";
            String columnsValues = "('"+cod_sct+"','"+capa+"','"+area_influencia+"',";

            for(String key: properties.keySet()){
                String value = properties.get(key).toString();

                value = value.replaceAll("'","''");
                if(key.equals("shape_json")){
                    columns+= "shape,";
                    columnsValues+= "ST_SetSRID(ST_GeomFromGeoJSON('"+value+"'),25830),";
                }else if(key.equals("intersection_area")){
                    columns+= "area_afeccion,";
                    columnsValues+= ""+Double.parseDouble(value)/10000+",";
                }else{
                    if(value.charAt(0)=='"'){
                        value = "'"+value.substring(1,value.length()-1)+"'";
                    }
                    columns+= "\""+key+"\""+",";
                    columnsValues+= ""+value+",";
                }

            }
            columns = columns.substring(0, columns.length()-1);
            columnsValues = columnsValues.substring(0, columnsValues.length()-1);

            columns+= ")";
            columnsValues+= ")";

            //log.info("INSERT INTO eota.\"t999_otinfo_geodata_"+tabla+"\" "+columns+" VALUES "+columnsValues+";");
            c.createStatement().execute("INSERT INTO eota.\"t999_otinfo_geodata_"+tabla+"\" "+columns+" VALUES "+columnsValues+";");
       

    }


}
