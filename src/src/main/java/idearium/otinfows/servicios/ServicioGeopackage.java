package idearium.otinfows.servicios;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.DataUtilities;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.geopkg.FeatureEntry;
import org.geotools.geopkg.GeoPackage;
import org.opengis.filter.Filter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

@Service
public class ServicioGeopackage {


    @Value("${spring.datasource.url}")
    String url;
    @Value("${spring.datasource.username}")
    String user;
    @Value("${spring.datasource.password}")
    String password;

    DataStore dataStore;

    @Autowired
    Util util;

    @Autowired
    ServicioBD servicioBD;

    @Value("${temp.path}")
    String tempPath;

    @Autowired
    ServicioFTP servicioFTP;

    private final static Logger log = LoggerFactory.getLogger(ServicioGeopackage.class.getName());

    @PostConstruct
    public void  inicialize() throws Exception {

        String[] parts = url.split("/");

        log.info(parts[3]);
        Map<String, Object> params = new HashMap<>();
        params.put("dbtype", "postgis");
        params.put("host", parts[2].split(":")[0]);
        params.put("port", 5432);
        params.put("schema", "eota");
        params.put("database", parts[3]);
        params.put("user", user);
        params.put("passwd", password);

        dataStore = DataStoreFinder.getDataStore(params);

    }
    private void addToGeopackage(GeoPackage geoPackage, String tabla, String codsct) {

        log.info("Añadiendo al geopackage tabla "+tabla);

        try{
            SimpleFeatureSource featureSource = dataStore.getFeatureSource(tabla);
            Filter filter = CQL.toFilter("cod_sct = '"+codsct+"'");

            FeatureEntry entry = new FeatureEntry();
            entry.setTableName(tabla);
            entry.setSrid(25830);

            SimpleFeatureCollection simpleCollection = DataUtilities.simple(featureSource.getFeatures(filter));
            geoPackage.add(entry, DataUtilities.source(simpleCollection), null);
        }catch (Exception e){
            util.logException(e);
        }
    }


    public String generarGeopackage(String cod_sct) throws Exception{

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));

        String ruta = tempPath+cod_sct+"_"+sdf.format(new Date())+".gpkg";
        File geo = new File(ruta);
        GeoPackage geoPackage = new GeoPackage(geo);
        geoPackage.init();

        ResultSet rs = servicioBD.execSQL("select distinct tabla_afecciones from eota.otinfo_afecciones where cod_sct='"+cod_sct+"'");
        while (rs.next()){
            String tabla = rs.getString("tabla_afecciones");

            addToGeopackage(geoPackage,"v999_otinfo_geodata_gpkg_"+tabla.substring(20),cod_sct);
        }

        addToGeopackage(geoPackage,"t999_otinfo_cod_sct",cod_sct);

        geoPackage.close();
        return ruta;
        

    }

}
