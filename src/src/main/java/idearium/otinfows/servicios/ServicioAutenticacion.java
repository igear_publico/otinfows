package idearium.otinfows.servicios;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.NodeList;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import javax.xml.soap.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.RSAPrivateKeySpec;
import java.util.Date;


@Service
public class ServicioAutenticacion {


	   @Autowired
	    Util util;

    @Value("${gestorUsuarios.url}")
    private String service_url;
    
	static private String CLIENT_SECRET = "";
	static private String ROL_APP = "ROL_G_OTINFO";

    public boolean validateToken(String token) throws Exception{
        try{
            URL url = new URL(service_url+"?REQUEST=VALIDATETOKEN&TOKEN="+token);
            Util.log.debug(service_url+"?REQUEST=VALIDATETOKEN&TOKEN="+token);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.connect();
            return (connection.getResponseCode()== HttpServletResponse.SC_OK && hasOTINFORol(token));
        }catch (Exception e){
            util.logException(e);
            throw e;
        }

    }
	protected static Claims parseToken(String jwt) throws Exception {
		
		//This line will throw an exception if it is not a signed JWS (as expected)
		Claims claims = Jwts.parser()         
				.setSigningKey(DatatypeConverter.parseBase64Binary(CLIENT_SECRET))
				.parseClaimsJws(jwt).getBody();
		//System.out.println("ID: " + claims.getId());
		return claims;

	
}
	protected static boolean hasOTINFORol(String token) throws Exception{
		Claims claims = parseToken(token);
		Object resultado = claims.get(ROL_APP);
		return((resultado!=null)&&(resultado.toString().equalsIgnoreCase("true")));
			
	
	}
}
