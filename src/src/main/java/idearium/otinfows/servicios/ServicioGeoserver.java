package idearium.otinfows.servicios;

import static org.springframework.http.HttpStatus.OK;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;

import org.postgresql.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ServicioGeoserver {

    private RestTemplate rest;

    DocumentBuilder xmlBuilder;

    @Autowired
    RestTemplateBuilder builder;

    @Autowired
    ServicioBD servicioBD;

    @Autowired
    Util util;

    private final static Logger log = LoggerFactory.getLogger(ServicioGeoserver.class.getName());


    int width = 410;
    int height = 410;

    int widthLegend = 20;
    int heightLegend = 20;
    String params = "?service=WMS&request=GetMap&width="+width+"&height="+height+"&srs=EPSG:25830&format=image/png";
    String paramsLegend = "?service=WMS&request=GetLegendGraphic&legend_options=forceLabels:on&width="+widthLegend+"&height="+heightLegend+"&srs=EPSG:25830&format=image/png";

    @Value("${geoserver.url}")
    String urlGeoserver;

    @Value("${geoserver.user}")
    String userGeoserver;
    
    @Value("${geoserver.pwd}")
    String pwdGeoserver;
    
    public List<String> obtenerUrlMapa(String expediente, String marcador){
        List<String> listado = new ArrayList<>();
        try {
            ResultSet rs = servicioBD.obtenerMapa(marcador);

            log.info(marcador);
            String area_influencia = servicioBD.obtenerBufferImagen(marcador);
            String[] parts = servicioBD.obtenerBoundingBox(expediente,area_influencia).split(",");


            String bbox = adaptBbox(Double.parseDouble(parts[0]),Double.parseDouble(parts[1]),Double.parseDouble(parts[2])
                    ,Double.parseDouble(parts[3]),width,height);

            if(rs!=null){
                while (rs.next()){
                    boolean fondo = rs.getBoolean("fondo");
                    String style = rs.getString("styles");
                    if(style ==null){
                        style="";
                    }
                    if(fondo){
                        listado.add(rs.getString("url")+params+"&version="+rs.getString("version")+"&transparent=true&STYLES="+style+"&layers="+rs.getString("layers") + "&bbox="+bbox);
                    }else{

                        String tabla = rs.getString("layers");
                        if(servicioBD.afecta(tabla,area_influencia,expediente)){
                            listado.add(rs.getString("url")+params+"&version="+rs.getString("version")+"&transparent=true&STYLES="+style+"&layers="+rs.getString("layers")+"&CQL_FILTER=(cod_sct='"+expediente+"')" + "&bbox="+bbox);
                        }
                    }
                }
            }
        }catch (Exception e){
            util.logException(e);
        }finally {
            return listado;
        }
    }


    public static String adaptBbox(double _minx,double _miny,double _maxx,double _maxy, double mwidth, double mheight) {



        double ratioTamano = mwidth / mheight;
        double diffX = _maxx - _minx;
        double diffY = _maxy - _miny;
        double ratioCoord = diffX / diffY;
        //alert("ratioT" + ratioTamano + "," + ratioCoord)
        if (ratioCoord > ratioTamano) {
            // estirar la Y
            //	alert("estirar la Y")

            double newDiffY = diffX / ratioTamano;

            double gapY = newDiffY - diffY;

            if (gapY > 0) {
                _miny = _miny - (gapY/2);
                _maxy = _maxy + (gapY/2);
            } else {
                _maxy = _miny - (gapY/2);
                _miny = _maxy + (gapY/2);
            }
        } else if (ratioCoord < ratioTamano){
            // estirar la X
            //alert("estirar la X")

            double newDiffX = ratioTamano * diffY;

            double gapX = newDiffX - diffX;
            if (gapX > 0) {
                _minx = _minx - (gapX/2);
                _maxx = _maxx + (gapX/2);
            } else {
                _maxx = _minx - (gapX/2);
                _minx = _maxx + (gapX/2);
            }
        }
        return _minx+","+ _miny+","+ _maxx+","+_maxy;
    }

    // para jpg

    public InputStream obtenerImagenConFondo(List<String> mapas){
        try{
            BufferedImage biResultado=null;
            Graphics g = null;


            for(String mapa: mapas){

                byte[] imagen = getGeoserverImage(new URL(mapa)).toByteArray();
                if(imagen!=null){
                    InputStream input = new ByteArrayInputStream(imagen);
                    BufferedImage imgFile = ImageIO.read(input);
                    if(biResultado==null){
                        biResultado = new BufferedImage(imgFile.getWidth(), imgFile.getHeight(), BufferedImage.TYPE_INT_ARGB); //suponiendo tamaño de imagen  y ARGB que soporta trasparencia
                        g = biResultado.getGraphics();
                    }


                    g.drawImage(imgFile, 0, 0, null);
                    input.close();
                }
            }

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(biResultado, "png", os);                          // Passing: ​(RenderedImage im, String formatName, OutputStream output)
            InputStream is = new ByteArrayInputStream(os.toByteArray());

            return is;
        }catch (Exception e){
            util.logException(e);
            return null;
        }
    }

    public List<byte[]> obtenerLeyendas(String marcador, String cod_sct){
        List<byte[]> listado = new ArrayList<>();
        try {
            ResultSet rs = servicioBD.obtenerMapa(marcador);


            if(rs!=null){
                while (rs.next()){

                    Boolean glg = rs.getBoolean("glg");
                    String leyenda = rs.getString("leyenda");

                    Boolean fondo = rs.getBoolean("fondo");

                    String area_influencia = rs.getString("area_influencia");
                    String tabla = rs.getString("layers");

                    if(!fondo){
                        if(servicioBD.afecta(tabla,area_influencia,cod_sct)){
                            if(leyenda!=null){
                                byte[] img = obtenerImagen(leyenda);
                                if(img!=null){
                                    listado.add(img);
                                }
                            }else{
                                if(glg !=null && glg){
                                    String style = rs.getString("styles");
                                    if(style ==null){
                                        style="";
                                    }
                                    String request=rs.getString("url")+paramsLegend+"&version="+rs.getString("version")+"&STYLES="+style+"&layer="+rs.getString("layers");

                                    byte[] img = getGeoserverImage(new URL(request)).toByteArray();
                                    if(img!=null){
                                        listado.add(img);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return listado;
        }catch (Exception e){
            util.logException(e);
        }finally {
            return listado;
        }
    }


    public ByteArrayOutputStream getFeature(String typename, String cql_filter, String sortBy) throws Exception{
        try{
            URL url = new URL(urlGeoserver+"?service=WFS&version=1.0.0&request=GetFeature&typename="+typename+
		"&outputFormat=application/json&srsname=EPSG:25830"+(cql_filter != null && cql_filter.length()>0?"&CQL_FILTER="+URLEncoder.encode(cql_filter):"")+
		(sortBy != null && sortBy.length()>0?"&sortBy="+sortBy:""));
            Util.log.debug(url.toString());
            String credentials = userGeoserver+":"+pwdGeoserver;
            //Util.log.debug(credentials);
        	String encoding = Base64.encodeBytes(credentials.getBytes());
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty  ("Authorization", "Basic " + encoding);
            InputStream in = (InputStream)connection.getInputStream();
            
            ByteArrayOutputStream response = new ByteArrayOutputStream();
                	 byte[] buff = new byte[1024];
     				int count;
     				
     				while ((count = in.read(buff)) > -1) {
     					response.write(buff, 0, count);
     					
     				}
            return response;
		
           
        }catch (Exception e){
            util.logException(e);
            throw e;
        }

    }

    public ByteArrayOutputStream getMap(String layers, String styles, String cql_filter, String format, int width, int height, String bbox) throws Exception{
        try{
            URL url = new URL(urlGeoserver+"?service=WMS&version=1.1.1&request=GetMap&LAYERS="+layers+"&format="+
            		(format != null && format.length()>0?format:"image/png")+"&TRANSPARENT=true"+(cql_filter != null && cql_filter.length()>0?"&CQL_FILTER="+URLEncoder.encode(cql_filter):"")
            		+"&SRS=EPSG%3A25830&STYLES="+(styles != null && styles.length()>0?styles:"")+"&WIDTH="+width+"&HEIGHT="+height+"&BBOX="+bbox);
            return getGeoserverImage(url);
		
           
        }catch (Exception e){
            util.logException(e);
            throw e;
        }

    }

    public ByteArrayOutputStream getLegendGraphic(String layer, String style) throws Exception{
        try{
            URL url = new URL(urlGeoserver+"?service=WMS&version=1.1.1&request=GetLegendGraphic&LAYER="+layer+"&format=image/png"+
        (style != null && style.length()>0?"&STYLE="+style:"")+"&width=20&height=20&legend_options=forceLabels:on;fontName:Lucida%20Sans%20Regular");
          return getGeoserverImage(url);
		
           
        }catch (Exception e){
            util.logException(e);
            throw e;
        }

    }
    
    public ByteArrayOutputStream getGeoserverImage(URL url) throws Exception{

    	 Util.log.debug(url.toString());
         String credentials = userGeoserver+":"+pwdGeoserver;
         Util.log.debug(credentials);
     	String encoding = Base64.encodeBytes(credentials.getBytes());
         HttpURLConnection connection = (HttpURLConnection)url.openConnection();
         connection.setRequestMethod("GET");
         connection.setDoOutput(true);
         connection.setRequestProperty  ("Authorization", "Basic " + encoding);
         InputStream in = (InputStream)connection.getInputStream();
         
         ByteArrayOutputStream response = new ByteArrayOutputStream();
             	 byte[] buff = new byte[1024];
  				int count;
  				
  				while ((count = in.read(buff)) > -1) {
  					response.write(buff, 0, count);
  					
  				}
  				
        if (connection.getContentType().startsWith("image")){

         return response;
        }
        else{
     	   throw new Exception("Error en la petición a Geoserver:"+
     			   response.toString());
        }
		

    }
    public byte[] obtenerImagen(String url){
        try{
            log.info(url);
            this.rest =  builder.build();
            byte[] imageBytes = rest.getForObject(url, byte[].class);
            return imageBytes;
        }catch (Exception e){
            log.info("ERROR::");
            util.logException(e);
            return null;
        }

    }
}
