package idearium.otinfows.servicios;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.PrintWriter;
import java.io.StringWriter;

@Service
public class Util {

    public final static Logger log = LoggerFactory.getLogger(Util.class.getName());

    public void logException(Exception e){
        log.error(exception2str(e));
    }
    
    public void logWarnException(Exception e){

        log.warn(exception2str(e));
    }

    public String exception2str(Exception e){
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString();
    }

}
