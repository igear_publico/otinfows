package idearium.otinfows.servicios;

import com.jcraft.jsch.*;
import idearium.otinfows.RestController;
import idearium.otinfows.modelo.Expediente;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@Service
public class ServicioFTP {


    private final static Logger log = LoggerFactory.getLogger(ServicioFTP.class.getName());


    @Value("${ftp.server}")
    public String ftp_server;
    @Value("${ftp.user}")
    public String ftp_user;
    @Value("${ftp.password}")
    public String ftp_password;
    @Value("${ftp.port}")
    public int ftp_port;


    public String idearagonFTP = "https://idearagon.aragon.es/otinfo/descarga_otinfo.php?file=";



    public String subirFichero(InputStream inputStream, String cod_sct, String extension) throws Exception{
        JSch jsch = new JSch();
       
            Session session = jsch.getSession( ftp_user, ftp_server, ftp_port );
            session.setConfig( "PreferredAuthentications", "password" );
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword( ftp_password );
            session.connect( 60000 );
            Channel channel = session.openChannel( "sftp" );
            ChannelSftp sftp = ( ChannelSftp ) channel;
            sftp.connect( 60000 );


            String url = null;


            try{
                sftp.mkdir("/ftp/otinfo/03_docs/"+cod_sct);
            }catch (Exception ex){ }

            try {
                sftp.lstat("/ftp/otinfo/03_docs/"+cod_sct+"/"+cod_sct+extension);

                //El fichero existe

                final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                sdf.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
                sftp.put(inputStream, "/ftp/otinfo/03_docs/"+cod_sct+"/"+cod_sct+"_"+sdf.format(new Date())+extension);

                url = idearagonFTP+cod_sct+"/"+cod_sct+"_"+sdf.format(new Date())+extension;
            } catch (SftpException e){
                if(e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE){
                    // El fichero no exixte
                    sftp.put(inputStream,"/ftp/otinfo/03_docs/"+cod_sct+"/"+cod_sct+extension);
                    url = idearagonFTP+cod_sct+"/"+cod_sct+extension;

                } else {
                    // Algo ha ido mal
                    throw e;
                }
            }

            sftp.exit();
            return url;

       
    }

}
