package idearium.otinfows.servicios;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import org.springframework.core.io.Resource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;

@Service
public class ServicioCalculosEspaciales {



    DocumentBuilder xmlBuilder;

    @Autowired
    RestTemplateBuilder builder;


    @Value("${url.spatialsearchservice}")
    String urlSpatialSearchService;

    String params = "?SERVICE=OTINFO&PROPERTYNAME=ALL,shape,INTERSECTION_AREA&TYPENAME=eota.t999_otinfo_cod_sct&INCLUDE_ERRORS=true&INCLUDE_NATIVE_TYPES=TRUE&STRING_VALUES=FALSE&CQL_FILTER=cod_sct=";

    @Autowired
    ServicioBD servicioBD;

    private RestTemplate rest;

    @Autowired
    Util util;


    private final static Logger log = LoggerFactory.getLogger(ServicioCalculosEspaciales.class.getName());



    public void realizarCalculos(String cod_sct) throws Exception{
       
    	
            this.rest =  builder.build();

            xmlBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            RestTemplate restTemplate = new RestTemplate();

            restTemplate.getMessageConverters()
                    .add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
            Util.log.debug(urlSpatialSearchService+params+"'"+cod_sct+"'");
            ResponseEntity<Resource> response = restTemplate.getForEntity(urlSpatialSearchService+params+"'"+cod_sct+"'", Resource.class);


            log.info("---------------");

            String capa="";
            String tabla="";
            String distancia="";
            String area_influencia = "";
            JsonObject properties;
            JsonArray native_types = null;

            InputStream responseInputStream;
            Gson gson = new Gson();


            responseInputStream = response.getBody().getInputStream();
            JsonReader reader = new JsonReader(new InputStreamReader(responseInputStream));
            reader.beginObject();
            boolean hayError=false;
            while (reader.hasNext()){
                String name = reader.nextName();
                if (name.equals("resultados")) {
                    //log.info("resultados");
                    reader.beginArray();
                    while (reader.hasNext()){
                        while (reader.hasNext()){

                            boolean initialized = false;
                            boolean table_error=false;
                            log.info("RESULTADO");
                            reader.beginObject();
                            while (reader.hasNext()){
                                String name2 = reader.nextName();
                                if (name2.equals("capa")) {
                                    capa=reader.nextString();
                                    String[] parts = capa.split("\\.",2);
                                    tabla = parts[1];
                                    tabla = tabla.replaceAll("\\\"","");
                                }else if(name2.equals("distancia")) {
                                    distancia =reader.nextString();
                                    if(distancia.equals("0")){
                                        area_influencia = "directa";
                                    }else if(distancia.equals("1000")){
                                        area_influencia = "1k";
                                    }else{
                                        area_influencia = "10k";
                                    }
                                    log.info("AREA_INFLUENCIA: "+area_influencia);

                                }else if(name2.equals("error")) {
                                	saveWarning( cod_sct,  capa,  distancia,!hayError,reader.nextString());
                                    hayError=true;
                                }else if(name2.equals("nativeTypes")) {

                                    log.info("NATIVE TYPES");
                                    native_types = gson.fromJson(reader, JsonArray.class);

                                    if(!initialized){
                                        try{
                                            //servicioBD.borrarTabla(capa);
                                            servicioBD.crearTabla(capa,native_types);
                                            servicioBD.crearVista(capa,native_types);
                                            initialized = true;
                                        }catch (Exception e){
                                            table_error=true;
                                            saveWarning( cod_sct,  capa,  distancia,!hayError,"No se ha podido crear la tabla eota.t999_otinfo_geodata_"+capa+" ("+util.exception2str(e)+")");
                                            hayError=true;

                                            util.logException(e);
                                        }

                                    }
                                    //log.info(native_types.get(1).toString());

                                }else if(name2.equals("featureCollection")){
                                        log.info("featureCollection: "+tabla+"   "+cod_sct+"    "+area_influencia);
                                        reader.beginObject();
                                        while (reader.hasNext() && !table_error){
                                            String name3 = reader.nextName();
                                            if (name3.equals("features")){
                                                reader.beginArray();
                                                int numFeatures = 0;
                                                while (reader.hasNext() && !table_error){
                                                    numFeatures++;
                                                    reader.beginObject();
                                                    while (reader.hasNext() && !table_error) {
                                                        String name4 = reader.nextName();
                                                        if(name4.equals("properties")) {
                                                            properties = gson.fromJson(reader, JsonObject.class);
                                                            if (initialized){
                                                            	 try{
                                                            	servicioBD.insertFeature(properties,cod_sct,capa,area_influencia,tabla);
                                                            	 }catch (Exception e){
                                                            		 saveWarning( cod_sct,  capa,  distancia,!hayError,"No se ha podido registrar la afección en eota.t999_otinfo_geodata_"+capa+" ("+util.exception2str(e)+")");
                                                                     hayError=true;
                                                                     util.logException(e);
                                                                 }
                                                            }
                                                        }else {
                                                            reader.skipValue();
                                                        }
                                                    }
                                                    reader.endObject();
                                                }
                                                if(numFeatures>0){
                                                    try{
                                                        servicioBD.guardarAfeccion(cod_sct,"t999_otinfo_geodata_"+tabla);
                                                    }catch (Exception e){
                                                        util.logException(e);
                                                    }
                                                }
                                                reader.endArray();
                                            }else{
                                                reader.skipValue();
                                            }
                                        }
                                        reader.endObject();
                                    }else{
                                        reader.skipValue();
                                    }
                                }

                            reader.endObject();
                        }
                    }
                    reader.endArray();
                }else {
                    reader.skipValue();
                }
            }

            reader.endObject();
            log.info("---------------");

       
    }


    private void saveWarning(String cod_sct, String capa, String distancia,boolean incluir_cabecera, String error_txt){
    	util.log.debug("Guardando warning:"+incluir_cabecera);
    	String error ="<li>"+capa+" (distancia="+distancia+"): "+error_txt+"</li>";
        if (incluir_cabecera){
        	error ="<span>No se han podido calcular o guardar las afecciones en las siguientes capas:<span><ul>"+error;
        }
        servicioBD. guardarWarning(cod_sct,error+"</ul>");
    }


}
