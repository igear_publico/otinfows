package idearium.otinfows;


import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.crypto.SecretKey;

import idearium.otinfows.modelo.*;
import idearium.otinfows.servicios.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.multipart.MultipartFile;

@org.springframework.web.bind.annotation.RestController
public class RestController {


	@Value("${logging.path}")
	String logpath;

	@Autowired
	ServicioBD servicioBD;

	@Autowired
	ServicioFTP servicioFTP;

	@Autowired
	ServicioAsincrono servicioAsincrono;

	@Autowired
	ServicioAutenticacion servicioAutenticacion;

	@Autowired
	ServicioGeoserver servicioGeoserver;

	@Autowired
	Util util;

	@Value("${temp.path}")
	String tempPath;

	SecretKey secretKey;

	@Autowired
	ServicioGeopackage servicioGeopackage;


	private final static Logger log = LoggerFactory.getLogger(RestController.class.getName());


	@RequestMapping(value = "/logs", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity logs() throws Exception {
		byte[] encoded = Files.readAllBytes(Paths.get(logpath+"spring.log"));
		String s = new String(encoded, StandardCharsets.US_ASCII);
		return ResponseEntity.status(OK).body(s);
	}

	@RequestMapping(value = "/clean", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity clean() throws Exception {
		PrintWriter writer = new PrintWriter(new File(logpath+"spring.log"));
		writer.print("");
		writer.close();
		return ResponseEntity.status(OK).body("OK");
	}


	@RequestMapping(value = "/afecciones", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity afecciones(@RequestParam(value = "TOKEN",required = true) String token, @RequestParam(value = "cod_sct", required = true) String cod_sct) throws Exception {
		try{
			if(servicioAutenticacion.validateToken(token)){
				CapaAfeccion[] afecciones = servicioBD.getAfecciones(cod_sct);
				return ResponseEntity.status(OK).body(afecciones);
			}else{
				return ResponseEntity.status(FORBIDDEN).body("Acceso denegado");
			}

		}
		catch(Exception ex){
			util.logException(ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}


	}
	@RequestMapping(value = "/novedades", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity novedades(@RequestParam(value = "TOKEN",required = true) String token, @RequestParam(value = "timestamp", required = true) Long timestamp) throws Exception {
		try{
			if(servicioAutenticacion.validateToken(token)){
				Novedad[] novedades = servicioBD.getNovedades(timestamp);
				return ResponseEntity.status(OK).body(novedades);
			}else{
				return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Acceso denegado");
			}
		}
		catch(Exception ex){
			util.logException(ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}


	}
	@RequestMapping(value = "/codelists", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity codelists(@RequestParam(value = "TOKEN",required = true) String token) throws Exception {
		try{
			if(servicioAutenticacion.validateToken(token)){
				ListaValores[] listas = servicioBD.getCodeLists();
				return ResponseEntity.status(OK).body(listas);
			}else{
				return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Acceso denegado");
			}
		}
		catch(Exception ex){
			util.logException(ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}

	@RequestMapping(value = "/regenerarInforme", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity regenerarInforme(@RequestParam(value = "TOKEN",required = true) String token,@RequestParam(value = "cod_sct", required = true) String cod_sct) throws Exception {
		try{
			if(servicioAutenticacion.validateToken(token)){
				Integer estado = servicioBD.getEstado(cod_sct);
				if (estado == null){
					return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body("El expediente no tiene estado asignado");
				}
				else if (estado>0){
					return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body("El expediente no está en un estado erróneo");
				}

				final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				sdf.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));

				servicioAsincrono.iniciarProceso(cod_sct,  estado);
				return ResponseEntity.status(OK).body("OK");
			}else{
				return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Acceso denegado");
			}
		}
		catch(Exception ex){
			util.logException(ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}
	@RequestMapping(value = "/generarInforme", method = { RequestMethod.GET, RequestMethod.POST },
			produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity generarInforme(@RequestParam(value = "TOKEN",required = true) String token,Expediente exp,
										 @RequestParam(value = "dbf_file", required = true) MultipartFile dbf_file,
										 @RequestParam(value = "shx_file", required = true) MultipartFile shx_file,
										 @RequestParam(value = "shp_file", required = true) MultipartFile shp_file,
										 @RequestParam(value = "proj_file", required = false) MultipartFile proj_file,
										 @RequestParam(value = "cpg_file", required = false) MultipartFile cpg_file
			) throws IOException {
		return generarInforme(token,exp,dbf_file,shx_file,shp_file,proj_file,cpg_file,"INSERT");
	}


	public ResponseEntity generarInforme(@RequestParam(value = "TOKEN",required = true) String token,Expediente exp,  MultipartFile dbf_file,
			  MultipartFile shx_file,
			 MultipartFile shp_file,
			MultipartFile proj_file,
			 MultipartFile cpg_file,
			String request) throws IOException {
		log.info("GENERANDO INFORME");
		try{
			if (!servicioAutenticacion.validateToken(token)){
				return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Acceso denegado");
			}
		}
		catch(Exception ex){
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error comprobando permisos del usuario");
		}
		
			final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			sdf.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
			exp.setFecha_mod(sdf.format(new Date()));

			exp = servicioBD.obtenerSolicitanteTxt(exp);

			log.info(exp.toString());
			try{
				boolean haySHP=false;
				if (request=="INSERT"){
					servicioBD.insertarExpediente(exp);
					haySHP=true;
				}
				else{
					servicioBD.actualizarExpediente(exp);
					haySHP =shp_file!=null;
				}
				if (haySHP){
					if ((shp_file == null)||(shx_file == null)||(dbf_file == null)){
						return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body("No están los 3 archivos requeridos del shapefile:shp,shx,dbf");
					}

					servicioBD.importarShp(exp,dbf_file,shx_file,shp_file,proj_file);

				}

				servicioAsincrono.iniciarProceso(exp,haySHP,request);
				return ResponseEntity.status(OK).body("OK");
			}

			catch(Exception ex){
				util.logException(ex);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error guardando los datos del expediente");
			}


		

	}

	@RequestMapping(value = "/actualizarInforme", method = { RequestMethod.GET, RequestMethod.POST },
			produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity actualizarInforme(@RequestParam(value = "TOKEN",required = true) String token,Expediente exp,
											@RequestParam(value = "dbf_file", required = false) MultipartFile dbf_file,
											@RequestParam(value = "shx_file", required = false) MultipartFile shx_file,
											@RequestParam(value = "shp_file", required = false) MultipartFile shp_file,
											@RequestParam(value = "proj_file", required = false) MultipartFile proj_file,
											@RequestParam(value = "cpg_file", required = false) MultipartFile cpg_file
			) throws IOException {
		return generarInforme(token, exp,dbf_file,shx_file,shp_file,proj_file,cpg_file,"UPDATE");
	}

	@RequestMapping(value = "/subirPDF", method = { RequestMethod.GET, RequestMethod.POST },
			produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity subirPDF(@RequestParam(value = "TOKEN",required = true) String token,@RequestParam(value = "file", required = true) MultipartFile file, @RequestParam(value = "cod_sct", required = true)String cod_sct) throws IOException {
		try{
			if (!servicioAutenticacion.validateToken(token)){
				return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Acceso denegado");
			}
		}
		catch(Exception ex){
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error comprobando permisos del usuario");
		}
		try{
			
				InputStream inputStream =  new BufferedInputStream(file.getInputStream());
				String url = servicioFTP.subirFichero(inputStream,cod_sct,".pdf");

				servicioBD.actualizarEstado(cod_sct,2);
				servicioBD.actualizarUrlPDF(cod_sct,url);
				return ResponseEntity.status(OK).body("OK");
			
		}
		catch(Exception ex){
			util.logException(ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("El fichero no ha podido subirse");
		}

	}


	@RequestMapping(value = "/generarGeopackage", method = { RequestMethod.GET, RequestMethod.POST },
			produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity generarGeopackage(@RequestParam(value = "TOKEN",required = true) String token,@RequestParam(value = "cod_sct", required = true)String cod_sct) throws IOException {
		try{
			if (!servicioAutenticacion.validateToken(token)){
				return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Acceso denegado");
			}
		}
		catch(Exception ex){
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error comprobando permisos del usuario");
		}

		try{
			servicioGeopackage.generarGeopackage(cod_sct);
			return ResponseEntity.status(OK).body("OK");
		}catch (Exception e){
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error generando el fichero geopackage");
		}

	}

	@RequestMapping(value = "/getFeature", method = { RequestMethod.GET, RequestMethod.POST },
			produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity getFeature(@RequestParam(value = "TOKEN",required = true) String token,@RequestParam(value = "typename", required = true) String typename,
			@RequestParam(value = "cql_filter", required = true)String cql_filter, @RequestParam(value = "sortBy", required = false)String sortBy) throws IOException {
		try{
			if (!servicioAutenticacion.validateToken(token)){
				return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Acceso denegado");
			}
		}
		catch(Exception ex){
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error comprobando permisos del usuario");
		}
		try{
			ByteArrayOutputStream response = servicioGeoserver.getFeature(typename, cql_filter, sortBy);
			return ResponseEntity.status(OK).body(response.toString("UTF-8"));
			
			
		}
		catch(Exception ex){
			util.logException(ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error haciendo la petición a Geoserver");
		}
	}





	@RequestMapping(value = "/getMap", method = { RequestMethod.GET, RequestMethod.POST })
	public ResponseEntity getMap(@RequestParam(value = "TOKEN",required = true) String token,@RequestParam(value = "LAYERS", required = true) String layers,
			@RequestParam(value = "STYLES", required = false)String styles,
			@RequestParam(value = "CQL_FILTER", required = false)String cql_filter, @RequestParam(value = "FORMAT", required = false)String format,
			@RequestParam(value = "WIDTH", required = true) int width,@RequestParam(value = "HEIGHT", required = true) int height,
			@RequestParam(value = "BBOX", required = true) String bbox
			) throws IOException {
		try{
			if (!servicioAutenticacion.validateToken(token)){
				return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Acceso denegado");
			}
		}
		catch(Exception ex){
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error comprobando permisos del usuario");
		}
		try{
			ByteArrayOutputStream response = servicioGeoserver.getMap(layers,styles,cql_filter,format,width,height,bbox);
			return ResponseEntity.status(OK).contentType((format!=null && (format.equalsIgnoreCase("image/jpeg")||format.equalsIgnoreCase("image/jpg"))?MediaType.IMAGE_JPEG:MediaType.IMAGE_PNG)).body(response.toByteArray());
			
			
		}
		catch(Exception ex){
			util.logException(ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}

	}

	@RequestMapping(value = "/getLegendGraphic", method = { RequestMethod.GET, RequestMethod.POST })
	public ResponseEntity getLegendGrapchi(@RequestParam(value = "TOKEN",required = true) String token,@RequestParam(value = "LAYER", required = true) String layer,
			@RequestParam(value = "STYLE", required = false)String style
			
			) throws IOException {
		try{
			if (!servicioAutenticacion.validateToken(token)){
				return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Acceso denegado");
			}
		}
		catch(Exception ex){
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error comprobando permisos del usuario");
		}
		try{
			ByteArrayOutputStream response = servicioGeoserver.getLegendGraphic(layer,style);
			return ResponseEntity.status(OK).contentType(MediaType.IMAGE_PNG).body(response.toByteArray());
			
			
		}
		catch(Exception ex){
			util.logException(ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}

	}
	
	@RequestMapping(value = "/borrarExpediente", method = { RequestMethod.GET, RequestMethod.POST },
			produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity borrarExpediente(@RequestParam(value = "TOKEN",required = true) String token,@RequestParam(value = "cod_sct",required = true)String cod_sct
			) throws IOException {
		try{
			if (!servicioAutenticacion.validateToken(token)){
				return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Acceso denegado");
			}
		}
		catch(Exception ex){
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error comprobando permisos del usuario");
		}
		try{
			servicioBD.borrarExpediente(cod_sct);
			return ResponseEntity.status(OK).body("OK");
			
			
		}
		catch(Exception ex){
			util.logException(ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}

}

