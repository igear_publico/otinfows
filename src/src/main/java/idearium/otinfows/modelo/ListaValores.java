package idearium.otinfows.modelo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ListaValores {


    @Expose
    private String nombre;

    @Expose
    private DominioListaValores[] valores;

        
    public ListaValores() {
    }



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public DominioListaValores[] getValores() {
		return valores;
	}



	public void setValores(DominioListaValores[] valores) {
		this.valores = valores;
	}



	@Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }


  
}
