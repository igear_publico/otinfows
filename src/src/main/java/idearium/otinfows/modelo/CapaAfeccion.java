package idearium.otinfows.modelo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CapaAfeccion {


    @Expose
    private String tema;

    @Expose
    private String titulo;

    @Expose
    private String tabla;

    @Expose
    private String campo_titulo;

    @Expose
    private String capa_origen;
    
    @Expose
    private Afeccion[] afecciones;


    public CapaAfeccion() {
    }

    


    public String getCampo_titulo() {
		return campo_titulo;
	}




	public void setCampo_titulo(String campo_titulo) {
		this.campo_titulo = campo_titulo;
	}




	public String getTema() {
		return tema;
	}




	public void setTema(String tema) {
		this.tema = tema;
	}




	public String getTitulo() {
		return titulo;
	}




	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}




	public String getTabla() {
		return tabla;
	}




	public void setTabla(String tabla) {
		this.tabla = tabla;
	}




	public String getCapa_origen() {
		return capa_origen;
	}




	public void setCapa_origen(String capa_origen) {
		this.capa_origen = capa_origen;
	}




	public Afeccion[] getAfecciones() {
		return afecciones;
	}




	public void setAfecciones(Afeccion[] afecciones) {
		this.afecciones = afecciones;
	}




	@Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }


    public static CapaAfeccion[] extraer(ResultSet rs) throws SQLException {
        List<CapaAfeccion> capas = new ArrayList<>();
        while (rs.next()){
        	CapaAfeccion e = new CapaAfeccion();
            e.setTabla(rs.getString("tabla"));
            e.setTema(rs.getString("tema"));
            e.setTitulo(rs.getString("titulo"));
            e.setCampo_titulo(rs.getString("campo_titulo"));
            e.setCapa_origen(rs.getString("capa_origen"));

            capas.add(e);
        }
        CapaAfeccion[] res = new CapaAfeccion[capas.size()];
        capas.toArray(res);
        return res;
    }


}
