package idearium.otinfows.modelo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Novedad {


  @Expose
   private String cod_sct;
	  
    @Expose
    private int estado;

    @Expose
    private String error;
    
    public Novedad() {
    }


	

	public String getCod_sct() {
		return cod_sct;
	}




	public void setCod_sct(String cod_sct) {
		this.cod_sct = cod_sct;
	}




	public int getEstado() {
		return estado;
	}




	public void setEstado(int estado) {
		this.estado = estado;
	}




	public String getError() {
		return error;
	}




	public void setError(String error) {
		this.error = error;
	}




	@Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }


    public static Novedad[] extraer(ResultSet rs) throws SQLException {
        List<Novedad> novedades = new ArrayList<>();
        while (rs.next()){
        	Novedad e = new Novedad();
           
            e.setCod_sct(rs.getString("cod_sct"));
            e.setEstado(rs.getInt("estado"));
            e.setError(rs.getString("error"));

            novedades.add(e);
        }
        Novedad[] res = new Novedad[novedades.size()];
        novedades.toArray(res);
        return res;
    }


}
