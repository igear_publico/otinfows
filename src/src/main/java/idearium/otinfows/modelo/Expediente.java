package idearium.otinfows.modelo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Expediente {


    @Expose
    private String title;

    @Expose
    private String cod_sct;

    @Expose
    private String date_ini;

    @Expose
    private boolean cota;

    @Expose
    private Boolean caducado;
    
    @Expose
    private Integer type_proy;

    @Expose
    private Integer solicitante;


    
    @Expose
    private String exp_solicitud;

    @Expose
    private String promotor;

    @Expose
    private String type_proc;

    @Expose
    private String solicitante_txt;

    @Expose
    private String fecha_mod;

    @Expose
    private String fecha_caducidad;
    
    @Expose
    private Integer estado;


    @Expose
    private String url_word;

    @Expose
    private String url_pdf;

    @Expose
    private String solicitante_txt_con_articulo;

    @Expose
    private String solicitante_subnivel;


    public Expediente() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCod_sct() {
        return cod_sct;
    }

    public void setCod_sct(String cod_sct) {
        this.cod_sct = cod_sct;
    }

    public String getDate_ini() {
        return date_ini;
    }

    public void setDate_ini(String date_ini) {
        this.date_ini = date_ini;
    }

    public boolean getCota() {
        return cota;
    }

    public void setCota(boolean cota) {
        this.cota = cota;
    }

    public Integer getType_proy() {
        return type_proy;
    }

    public void setType_proy(Integer type_proy) {
        this.type_proy = type_proy;
    }

    public Integer getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(Integer solicitante) {
        this.solicitante = solicitante;
    }

    public String getExp_solicitud() {
        return exp_solicitud;
    }

    public void setExp_solicitud(String exp_solicitud) {
        this.exp_solicitud = exp_solicitud;
    }

    public String getPromotor() {
        return promotor;
    }

    public void setPromotor(String promotor) {
        this.promotor = promotor;
    }

    public String getType_proc() {
        return type_proc;
    }

    public void setType_proc(String type_proc) {
        this.type_proc = type_proc;
    }

    public String getSolicitante_txt() {
        return solicitante_txt;
    }

    public void setSolicitante_txt(String solicitante_txt) {
        this.solicitante_txt = solicitante_txt;
    }


    public String getUrl_word() {
        return url_word;
    }

    public void setUrl_word(String url_word) {
        this.url_word = url_word;
    }

    public String getUrl_pdf() {
        return url_pdf;
    }

    public void setUrl_pdf(String url_pdf) {
        this.url_pdf = url_pdf;
    }
    public String getFecha_mod() {
        return fecha_mod;
    }

    public void setFecha_mod(String fecha_mod) {
        this.fecha_mod = fecha_mod;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }


    public String getSolicitante_txt_con_articulo() {
        return solicitante_txt_con_articulo;
    }

    public void setSolicitante_txt_con_articulo(String solicitante_txt_con_articulo) {
        this.solicitante_txt_con_articulo = solicitante_txt_con_articulo;
    }

    public String getSolicitante_subnivel() {
        return solicitante_subnivel;
    }

    public void setSolicitante_subnivel(String solicitante_subnivel) {
        this.solicitante_subnivel = solicitante_subnivel;
    }


    public Boolean getCaducado() {
		return caducado;
	}

	public void setCaducado(Boolean caducado) {
		this.caducado = caducado;
	}

	public String getFecha_caducidad() {
		return fecha_caducidad;
	}

	public void setFecha_caducidad(String fecha_caducidad) {
		this.fecha_caducidad = fecha_caducidad;
	}

	@Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }



    public String getFileName(){
        return cod_sct +"_"+ fecha_mod;
    }

}
