package idearium.otinfows.modelo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Afeccion {


    @Expose
    private String objectid;

    @Expose
    private String titulo;

    @Expose
    private String area_influencia;
    
    public Afeccion() {
    }


	public String getObjectid() {
		return objectid;
	}


	public void setObjectid(String objectid) {
		this.objectid = objectid;
	}

	public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getArea_influencia() {
		return area_influencia;
	}


	public void setArea_influencia(String area_influencia) {
		this.area_influencia = area_influencia;
	}


	@Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }


    public static Afeccion[] extraer(ResultSet rs) throws SQLException {
        List<Afeccion> afecciones = new ArrayList<>();
        while (rs.next()){
        	Afeccion e = new Afeccion();
           
            e.setObjectid(rs.getString("objectid"));
            e.setTitulo(rs.getString("titulo"));
            e.setArea_influencia(rs.getString("area_influencia"));

            afecciones.add(e);
        }
        Afeccion[] res = new Afeccion[afecciones.size()];
        afecciones.toArray(res);
        return res;
    }


}
