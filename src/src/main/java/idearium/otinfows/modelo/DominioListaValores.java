package idearium.otinfows.modelo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DominioListaValores {


    @Expose
    private String id;

    @Expose
    private String nombre;
   
    @Expose
    private String complemento;
    public DominioListaValores() {
    }


	

	public String getId() {
		return id;
	}




	public void setId(String id) {
		this.id = id;
	}




	public String getNombre() {
		return nombre;
	}




	public void setNombre(String nombre) {
		this.nombre = nombre;
	}




	public String getComplemento() {
		return complemento;
	}




	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}




	@Override
    public String toString() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }


    public static DominioListaValores[] extraer(ResultSet rs) throws SQLException {
        List<DominioListaValores> valores = new ArrayList<>();
        while (rs.next()){
        	DominioListaValores e = new DominioListaValores();
           
            e.setId(rs.getString("id"));
            e.setNombre(rs.getString("nombre"));
            try{
            e.setComplemento(rs.getString("complemento"));
            }
            catch(SQLException ex){
            	// no pasa nada, la lista no tiene complemneto
            }

            valores.add(e);
        }
        DominioListaValores[] res = new DominioListaValores[valores.size()];
        valores.toArray(res);
        return res;
    }


}
